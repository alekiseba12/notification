<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatatuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matatu', function (Blueprint $table) {
            $table->bigInteger('mat_no')->unique()->primaryKey();
            $table->string('name');
            $table->string('no_plate')->unique();
            $table->string('mat_routes');
            $table->integer('passengers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matatu');
    }
}
