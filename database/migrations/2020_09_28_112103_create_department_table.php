<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('department', function (Blueprint $table) {
            $table->bigInteger('department_No')->primary();
            $table->string('department_Name');
            $table->string('pic');
            $table->string('description');
            $table->boolean('status')->default(0);
            $table->string('reason')->nullable();
            $table->integer('percentage')->nullable();
            $table->integer('employees');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('department');
    }
}
