<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advert', function (Blueprint $table) {
            $table->bigInteger('advert_No')->unique()->primaryKey();
            $table->string('advert_name');
            $table->string('advert_period_time');
            $table->string('location');
            $table->string('status')->default(0);
            $table->bigInteger('client_No');
            $table->foreign('client_No')->references('client_No')->on('client')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advert');
    }
}
