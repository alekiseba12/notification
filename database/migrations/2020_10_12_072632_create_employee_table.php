<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {
            $table->bigInteger('employee_Number')->unique()->primary();
            $table->string('fullnames');
            $table->string('gender');
            $table->string('phone');
            $table->string('address');
            $table->string('location');
            $table->integer('national_id')->unique();
            $table->string('salary')->nullable();
            $table->string('type')->nullable();
            $table->string('pic');
            $table->boolean('status')->default(0);
            $table->text('description');
            $table->string('education');
            $table->string('skills');
            $table->string('email');
            $table->bigInteger('role_no')->nullable();
            $table->foreign('role_no')->references('role_no')->on('role')->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('department_No')->nullable();
            $table->foreign('department_No')->references('department_No')->on('department')->onUpdate('cascade')->onDelete('cascade');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee');
    }
}
