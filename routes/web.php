<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify'=>true]);


Route::post('custom.login', 'LoginController@login')->name('custom.login');

/*** 
 **** Admin Routes
*/
       //Employee Routes

        Route::get('admin', 'Admin\IndexController@isAdmin')->name('admin');

        Route::get('employee-registration-form', 'Admin\EmployeeController@index')->name('employee-registration-form');

        Route::post('create-employee', 'Admin\EmployeeController@create')->name('create-employee');

        Route::get('management-team','Admin\EmployeesController@index')->name('management-team');

        Route::get('employee-profile/{employee_Number}','Admin\EmployeeController@show');

        Route::get('edit-employee-profile/{employee_Number}','Admin\EmployeeController@edit');

        Route::post('update-employee-profile/{employee_Number}','Admin\EmployeeController@update');

        Route::post('delete-employee-profile/{employee_Number}','Admin\EmployeeController@delete');

        //End of Employee Routes


       
       //Client Routes

        Route::get('client-registration-form', 'Admin\Clients\ClientController@index')->name('client-registration-form');

        Route::post('create-client', 'Admin\Clients\ClientController@create')->name('create-client');

        Route::get('view-clients','Admin\Clients\ClientsController@clients')->name('view-clients');

        Route::post('update-client/{client_No}','Admin\Clients\ClientController@update');

        Route::post('delete-client/{client_No}','Admin\Clients\ClientController@delete');

        //End of Client Routes

        //Services Routes
        Route::get('new-service', 'Admin\Services\ServiceController@index')->name('new-service');

        Route::post('create-service', 'Admin\Services\ServiceController@create')->name('create-service');

        Route::post('update-service/{service_No}','Admin\Services\ServiceController@update');

        Route::post('delete-service/{service_No}','Admin\Services\ServiceController@delete');



      
      //Departments Routes

        Route::get('new-department','Admin\Departments\DepartmentController@index')->name('new-department');

        Route::post('create-department', 'Admin\Departments\DepartmentController@create')->name('create-department');

        Route::post('update-department/{department_No}','Admin\Departments\DepartmentController@update');

        Route::post('delete-department/{department_No}','Admin\Departments\DepartmentController@delete');




        //Gadgets Routes
        Route::get('new-gadget','Admin\Gadgets\GadgetController@index')->name('new-gadget');

        Route::post('create-gadget', 'Admin\Gadgets\GadgetController@create')->name('create-gadget');

        Route::post('update-gadget/{gadget_code}','Admin\Gadgets\GadgetController@update');

        Route::post('delete-gadget/{gadget_code}','Admin\Gadgets\GadgetController@delete');



        //Contracts Routes
        Route::get('new-contract','Admin\Employments\EmploymentController@index')->name('new-contract');

        Route::post('create-contract', 'Admin\Employments\EmploymentController@create')->name('create-contract');

        Route::post('update-contract/{employment_No}','Admin\Employments\EmploymentController@update');

        Route::post('delete-contract/{employment_No}','Admin\Employments\EmploymentController@delete');





        //Matatus Routes
        Route::get('new-matatu','Admin\Matatus\MatatuController@index')->name('new-matatu');

        Route::post('create-matatu', 'Admin\Matatus\MatatuController@create')->name('create-matatu');

        Route::post('update-matatu/{mat_no}','Admin\Matatus\MatatuController@update');

        Route::post('delete-matatu/{mat_no}','Admin\Matatus\MatatuController@delete');



        //Adverts Routes
        Route::get('new-advert','Admin\Adverts\AdvertController@index')->name('new-advert');

        Route::post('create-advert', 'Admin\Adverts\AdvertController@create')->name('create-advert');

        Route::post('update-advert/{advert_No}','Admin\Adverts\AdvertController@update');

        Route::post('delete-advert/{advert_No}','Admin\Adverts\AdvertController@delete');


        //Taks Routes
        Route::get('new-task','Admin\Tasks\TaskController@index')->name('new-task');

        Route::post('create-task', 'Admin\Tasks\TaskController@create')->name('create-advert');

        Route::post('update-task/{id}','Admin\Tasks\TaskController@update');

        Route::post('delete-task/{id}','Admin\Tasks\TaskController@delete');


        
        //Roles Routes
        Route::get('new-role', 'Admin\Roles\RoleController@index')->name('new-role');

        Route::post('create-role', 'Admin\Roles\RoleController@create')->name('create-role');

        Route::post('update-role/{role_no}','Admin\Roles\RoleController@update');

        Route::post('delete-role/{role_no}','Admin\Roles\RoleController@delete');




        //Stock Routes
        Route::get('nyce_bar_stock_exchange', 'Admin\Stock\StockController@index')->name('nyce_bar_stock_exchange');

        Route::post('create-county', 'Admin\Stock\Counties\CountyController@create')->name('create-county');

        Route::post('create-region', 'Admin\Stock\Regions\RegionController@create')->name('create-region');

        Route::post('create-new-bar', 'Admin\Stock\Bars\BarController@create')->name('create-new-bar');

        Route::post('create-new-product', 'Admin\Stock\Products\ProductController@create')->name('create-new-product');




       //Routes to get the stock menus
        Route::get('all-bars', 'Admin\Stock\Bars\BarsController@index')->name('all-bars');

        Route::get('all-bar-products/{bar_no}', 'Admin\Stock\Bars\BarController@viewProduct');
        
        Route::get('all-regions', 'Admin\Stock\Regions\RegionsController@index')->name('all-regions');
        Route::get('all-region-bars/{region_no}', 'Admin\Stock\Regions\RegionController@viewBars');

        Route::get('all-counties', 'Admin\Stock\Counties\CountiesController@index')->name('all-counties');
        Route::get('all-county-regions/{county_no}', 'Admin\Stock\Counties\CountyController@viewRegions');



        //Contacts Admin

        Route::get('get-contacts', 'Admin\IndexController@getContacts')->name('get-contacts');



        //End Routes for Admin



        /*** 
         **** User Routes
        */
        //Index dashboard
	   Route::get('/home', 'HomeController@index')->name('home');	

       Route::get('activation', 'HomeController@activate')->name('activation');

       //Attendance Route

       //Route::post('submit-attendance', 'Users\attendanceController@create')->name('submit-attendance');

       //Updating the  Route

       Route::post('edit-task/{id}', 'Users\TasksController@edit');

       Route::post('response-task/{id}', 'Users\TasksController@response');

       Route::post('reason-task/{id}', 'Users\TasksController@reason');



       Route::post('submit', 'Users\attendanceController@create')->name('submit');














       Route::post('create_booking', 'HomeController@create')->name('create_booking');
       Route::get('user-bookings', 'Users\BookingController@services')->name('user-bookings');


