<?php

namespace App\Traits;
use App\Models\Admin\Gadgets\gadget;
use App\User;
use Auth;
use DB;

trait GadgetTrait
{
	/**
     * Retrieve all  bookings 
     * Note: this was build pre laravel booking.
     *
     * @param  User $user
     *
     * @return void
     */
	public function getGadgets(){
		$gadgets=gadget::all()->count();

		return $gadgets;
	}

     public function allGadget(){
          $allGadgets=gadget::all();

          return $allGadgets;
     }
 }
 
?>