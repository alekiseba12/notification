<?php

namespace App\Traits;
use App\Models\Admin\Tasks\task;
use App\User;
use Auth;
use DB;

trait taskTrait
{
	/**
     * Retrieve all  bookings 
     * Note: this was build pre laravel booking.
     *
     * @param  User $user
     *
     * @return void
     */
	public function getTasks(){
		$tasks=task::all()->count();

		return $tasks;
	}

     public function allTasks(){
          $companyTasks=DB::table('task')
                               ->join('users','users.id','=','task.user_id')
                               ->select('task.*','users.name')
                               ->get();

          return $companyTasks;
     }
    public function getAllUsers(){
    $taskUsers=User::all();

    return $taskUsers;
  }
 }
 
?>