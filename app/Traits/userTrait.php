<?php

namespace App\Traits;
use App\Models\Admin\Services\service;
use App\Models\Admin\Tasks\task;
use App\Models\Admin\employee;
use App\User;
use Auth;
use DB;

trait userTrait
{
	/**
     * Retrieve all  bookings 
     * Note: this was build pre laravel user.
     *
     * @param  User $user
     *
     * @return void
     */
	public function getLoggedUser(){

    $user=Auth::User();
		$employee=DB::table('employee')
              ->join('users','users.id','=','employee.user_id')
              ->join('role','role.role_No','=','employee.role_No')
              ->join('department','department.department_No','=','employee.department_No')
              ->select('employee.*','users.*','department.department_Name','role.role')
              ->where('employee.user_id',$user['id'])
              ->first();

		return $employee;
	}
  //get department

  public function department(){
    $user=Auth::User();
    $getDepartment=DB::table('employee')
              ->join('users','users.id','=','employee.user_id')
              ->join('department','department.department_No','=','employee.department_No')
              ->select('department.*')
              ->where('employee.user_id',$user['id'])
              ->first();

    return $getDepartment;


  }
  //Task Assigned to Employee
    public function employeeTasks(){
    $user=Auth::User();
    $getTask=DB::table('task')
              ->join('users','users.id','=','task.user_id')
              ->select('task.*')
              ->where('task.user_id',$user['id'])
              ->first();

    return $getTask;

  }

  //user Contracts
   public function contracts(){
        $user=Auth::User();
        $userContracts=DB::table('contracts')
                       ->join('users','users.id','=','contracts.user_id')
                       ->select('contracts.*','users.*')
                       ->where('contracts.user_id',$user['id'])
                       ->first();
          return $userContracts;
     }

     //send the email

     public function taskEmail(){

          $getEmail=DB::table('task')
              ->join('users','users.id','=','task.user_id')
              ->select('users.email')
              ->first();

    return $getEmail;

  }


 }
 
?>