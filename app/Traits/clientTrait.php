<?php

namespace App\Traits;
use App\Models\Admin\Clients\client;
use App\User;
use Auth;
use DB;

trait ClientTrait
{
	/**
     * Retrieve all  bookings 
     * Note: this was build pre laravel booking.
     *
     * @param  User $user
     *
     * @return void
     */
	public function getClients(){
		$clients=client::all()->count();

		return $clients;
	}

     public function allClients(){
          $allClients=client::all()->take(5);

          return $allClients;
     }

     public function clientsActive(){

               $actives=client::where('status','=',1)->get();

               return $actives;


     }
     public function clientsInactive(){

               $inactives=client::where('status','=',2)->get();

               return $inactives;


     }
 }
 
?>