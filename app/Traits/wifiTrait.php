<?php

namespace App\Traits;
use App\Models\Admin\Wifis\wifi;
use App\User;
use Auth;
use DB;

trait wifiTrait
{
	/**
     * Retrieve all  bookings 
     * Note: this was build pre laravel booking.
     *
     * @param  User $user
     *
     * @return void
     */
	public function getWifi(){
		$wifi=wifi::all()->count();

		return $wifi;
	}

     public function allWifi(){
          $companyWifi=wifi::all();

          return $companyWifi;
     }
 }
 
?>