<?php

namespace App\Traits;
use App\Models\Admin\Departments\department;
use App\User;
use Auth;
use DB;

trait departmentTrait
{
	/**
     * Retrieve all  bookings 
     * Note: this was build pre laravel booking.
     *
     * @param  User $user
     *
     * @return void
     */
	public function getDepartments(){
		$departments=department::all()->count();

		return $departments;
	}

     public function allDepartments(){
          $allDepartments=department::all();

          return $allDepartments;
     }
 }
 
?>