<?php

namespace App\Traits;
use App\Models\booking;
use App\User;
use Auth;
use DB;

trait BookingTrait
{
	/**
     * Retrieve all  bookings 
     * Note: this was build pre laravel booking.
     *
     * @param  User $user
     *
     * @return void
     */
	public function getBookings(){
		$bookings=booking::all();

		return $bookings;
	}
     public function userBooks(){

          $currentUser=Auth::User();

          $userBooking=DB::table('bookings')
                         ->join('users','users.id','=', 'bookings.user_id' )
                         ->select('users.name', 'bookings.company', 'bookings.service', 'bookings.phone', 'bookings.email','bookings.postcode', 'users.email')
                         ->where('bookings.user_id', '=', $currentUser['id'])
                         ->get();
          if ($userBooking==null) {
                
                return "No Data to be found!";
          }
          return $userBooking;
     }

	}
?>