<?php

namespace App\Traits;
use App\Models\Admin\Stocks\Counties\county;
use App\Models\Admin\Stocks\Regions\region;
use App\Models\Admin\Stocks\Bars\bar;
use App\Models\Admin\Stocks\Products\product;
use App\User;
use Auth;
use DB;

trait stockTrait
{
	/**
     * Retrieve all  bookings 
     * Note: this was build pre laravel booking.
     *
     * @param  User $user
     *
     * @return void

     *///Counties Model
	public function getCounties(){
		$counties=county::all()->count();

		return $counties;
	}

     public function allCounties(){
          $stockCounties=county::all();

          return $stockCounties;
     }
       public function limitCounties(){
          $limitCounties=county::all()->take(3);

          return $limitCounties;
     }

     //Regions Model
     public function getRegions(){
          $regions=region::all()->count();

          return $regions;
     }

     public function allRegions(){
          $stockRegions=region::all();

          return $stockRegions;
     }

       public function limitRegions(){
          $limitRegion=region::all()->take(3);

          return $limitRegion;
     }



     //Bars  Model
     public function getBars(){
          $bars=bar::all()->count();

          return $bars;
     }

     public function allBars(){
          $stockBars=bar::all();

          return $stockBars;
     }

      public function limitBars(){
          $limitBar=bar::all()->take(3);

          return $limitBar;
     }


     //Products  Model
     public function getProducts(){
          $products=product::all()->count();

          return $products;
     }

     public function allProducts(){
          $stockProducts=product::all();

          return $stockProducts;
     }
       public function limitProducts(){
          $limitProduct=product::all()->take(3);

          return $limitProduct;
     }

 }
 
?>