<?php

namespace App\Traits\Stock;
use App\Models\Admin\Stocks\Bars\bar;
use App\Models\Admin\Stocks\Regions\region;
use App\User;
use Auth;
use DB;

trait regionsTrait
{
	/**
     * Retrieve all  bookings 
     * Note: this was build pre laravel booking.
     *
     * @param  User $user
     *
     * @return void
     */
	public function getAllRegions(){
		$regions=region::all();

		return $regions;
	}

     public function getRegion($region_no){

          $bar=DB::table('bar')->join('region', 'region.region_no', '=','bar.region_no')->select('bar.*')->where('bar.region_no','=',$region_no)->get();

          return $bar;
     }
 }
 
?>