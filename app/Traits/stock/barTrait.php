<?php

namespace App\Traits\Stock;
use App\Models\Admin\employee;
use App\Models\Admin\Stocks\Bars\bar;
use App\Models\Admin\Stocks\Products\product;
use App\User;
use Auth;
use DB;

trait barTrait
{
	/**
     * Retrieve all  bookings 
     * Note: this was build pre laravel booking.
     *
     * @param  User $user
     *
     * @return void
     */
	public function getAllBars(){
		$bars=bar::all();

		return $bars;
	}

     public function getBar($bar_no){

          $bar=DB::table('product')->join('bar', 'bar.bar_no', '=','product.bar_no')->select('product.*','bar.*')->where('bar.bar_no','=',$bar_no)->get();

          return $bar;
     }
 }
 
?>