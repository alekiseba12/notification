<?php

namespace App\Traits\Stock;
use App\Models\Admin\Stocks\Counties\county;
use App\Models\Admin\Stocks\Regions\region;
use App\User;
use Auth;
use DB;

trait countyTrait
{
	/**
     * Retrieve all  bookings 
     * Note: this was build pre laravel booking.
     *
     * @param  User $user
     *
     * @return void
     */
	public function getAllCounties(){
		$counties=county::all();

		return $counties;
	}

     public function getCounty($county_no){

          $region=DB::table('region')->join('county', 'county.county_no', '=','region.county_no')->select('region.*')->where('region.county_no','=',$county_no)->get();

          return $region;
     }
 }
 
?>