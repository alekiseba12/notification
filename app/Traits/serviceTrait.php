<?php

namespace App\Traits;
use App\Models\Admin\Services\service;
use App\User;
use Auth;
use DB;

trait ServiceTrait
{
	/**
     * Retrieve all  bookings 
     * Note: this was build pre laravel booking.
     *
     * @param  User $user
     *
     * @return void
     */
	public function getServices(){
		$services=service::all()->count();

		return $services;
	}

     public function allServices(){
          $companyServices=DB::table('service')
                               ->join('employee','employee.employee_Number','=','service.employee_Number')
                               ->select('service.*','employee.fullnames')
                               ->get();

          return $companyServices;
     }
 }
 
?>