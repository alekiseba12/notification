<?php

namespace App\Traits;
use App\Models\Admin\Departments\department;
use App\Models\Admin\Roles\role;
use App\User;
use Auth;
use DB;

trait roleTrait
{
	/**
     * Retrieve all  bookings 
     * Note: this was build pre laravel booking.
     *
     * @param  User $user
     *
     * @return void
     */
	public function getRoles(){
		$roles=role::all()->count();

		return $roles;
	}

     public function allRoles(){
          $allRoles=role::all();

          return $allRoles;
     }
 }
 
?>