<?php

namespace App\Traits;
use App\Models\Admin\Adverts\advert;
use App\User;
use Auth;
use DB;

trait AdvertTrait
{
	/**
     * Retrieve all  bookings 
     * Note: this was build pre laravel booking.
     *
     * @param  User $user
     *
     * @return void
     */
	public function getAdverts(){
		$adverts=advert::all()->count();

		return $adverts;
	}

     public function allAdverts(){
          $companyAdverts=advert::all();

          return $companyAdverts;
     }

     public function countsActive(){

          $countsActiveStatus=advert::all()
               ->where('status','=',1)
               ->count();

          return $countsActiveStatus;
     }
      public function countsInactive(){

          $countsInactiveStatus=advert::all()
               ->where('status','=',2)
               ->count();

          return $countsInactiveStatus;
     }
      public function countsCompleted(){
          $countsCompletedStatus=advert::all()
               ->where('status','=',3)
               ->count();
          return $countsCompletedStatus;
     }
     
          public function countsDeclined(){

          $countsDeclinedtatus=advert::all()
               ->where('status','=',0)
               ->count();
          return $countsDeclinedtatus;
     }

 }
 
?>