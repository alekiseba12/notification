<?php

namespace App\Traits;
use App\Models\Admin\employee;
use App\User;
use Auth;
use DB;

trait EmployeeTrait
{
	/**
     * Retrieve all  bookings 
     * Note: this was build pre laravel booking.
     *
     * @param  User $user
     *
     * @return void
     */
	public function getAllEmployees(){
		$employees=employee::all()->count();

		return $employees;
	}
     public function managementTeam(){
          $team=employee::all();

          return $team;

     }
     public function allEmployees(){
          $employee=employee::all();

          return $employee;
     }
     public function getLoggedinEmployee(){

          $user=Auth::user();

          $getEmployee=DB::table('employee')
                       ->join('users','users.id','=','employee.user_id')
                       ->select('employee.*','users.*')
                       ->where('employee.user_id',$user['id'])
                       ->first();
                       return $getEmployee;
     }

 }
 
?>