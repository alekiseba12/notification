<?php

namespace App\Traits;
use App\Models\Admin\Employments\contracts;
use App\User;
use Auth;
use DB;

trait employmentTrait
{
	/**
     * Retrieve all  bookings 
     * Note: this was build pre laravel booking.
     *
     * @param  User $user
     *
     * @return void
     */
	public function getEmployment(){
		$employments=contracts::all()->count();

		return $employments;
	}

     public function allEmployments(){
          $allEmployments=DB::table('contracts')
                       ->join('users','users.id','=','contracts.user_id')
                       ->select('contracts.*','users.*')
                       ->get();

          return $allEmployments;
     }
     
 }
 
?>