<?php

namespace App\Traits;
use App\Models\Admin\Matatus\matatu;
use App\User;
use Auth;
use DB;

trait matatuTrait
{
	/**
     * Retrieve all  bookings 
     * Note: this was build pre laravel booking.
     *
     * @param  User $user
     *
     * @return void
     */
	public function getMatatus(){
		$matatus=matatu::all()->count();

		return $matatus;
	}

     public function allMatatus(){
          $allMatatus=matatu::all();

          return $allMatatus;
     }
 }
 
?>