<?php

namespace App\Models\User\Attendance;

use Illuminate\Database\Eloquent\Model;

class attendance extends Model
{
   //display the table for advert

    protected $table='attendance';

    //Define the primary key column

    protected $primaryKey='id';

    //Fill the model with the  attributes

    protected $fillable=[
    	'attendance_date',
    	'attendance_day',
    	'time_in',
    	'time_out',
    	'user_id',
    	
    ];
    
}
