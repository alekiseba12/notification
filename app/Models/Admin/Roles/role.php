<?php

namespace App\Models\Admin\Roles;

use Illuminate\Database\Eloquent\Model;

class role extends Model
{
    
     //Define the role table
    protected $table='role';

    //Set the primary key for a column

    protected $primaryKey = 'role_no';

    //Fill the model with the data from the user inputs

    protected $fillable=[

    	     'role_no', 
    	     'role',
    	     	     
    ];
}
