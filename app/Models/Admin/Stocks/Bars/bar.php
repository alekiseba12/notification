<?php

namespace App\Models\Admin\Stocks\Bars;

use Illuminate\Database\Eloquent\Model;

class bar extends Model
{
    //Define the county table
    protected $table='bar';

    //Set the primary key for a column

    protected $primaryKey = 'bar_no';

    //Fill the model with the data from the user inputs

    protected $fillable=[

    	     'bar_no', 
    	     'bar_name',
    	     'region_no'
    	     	     
    ];
    public function products()
    {
        return $this->hasMany('App\Models\Admin\Stock\Products\product');
    }

    public function region()
    {
     return $this->belongsTo('App\Models\Admin\Stocks\Regions\region');
    }

}
