<?php

namespace App\Models\Admin\Stocks\Regions;

use Illuminate\Database\Eloquent\Model;

class region extends Model
{
     //Define the county table
    protected $table='region';

    //Set the primary key for a column

    protected $primaryKey = 'region_no';

    //Fill the model with the data from the user inputs

    protected $fillable=[

    	     'region_no', 
    	     'region_name',
    	     'county_no',
    	     	     
    ];
    public function bars()
    {
       return $this->hasMany('App\Models\Admin\Stock\Bars\bar');
    }

    public function county()
    {
     return $this->belongsTo('App\Models\Admin\Stocks\Counties\county');
    }
}
