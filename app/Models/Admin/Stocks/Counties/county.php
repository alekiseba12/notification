<?php

namespace App\Models\Admin\Stocks\Counties;

use Illuminate\Database\Eloquent\Model;

class county extends Model
{
    //Define the county table
    protected $table='county';

    //Set the primary key for a column

    protected $primaryKey = 'county_no';

    //Fill the model with the data from the user inputs

    protected $fillable=[

    	     'county_no', 
    	     'county',
    	     	     
    ];
    public function regions()
    {
       return $this->hasMany('App\Models\Admin\Stock\Regions\region');
    }
}