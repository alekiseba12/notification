<?php

namespace App\Models\Admin\Stocks\Products;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    //Define the county table
    protected $table='product';

    //Set the primary key for a column

    protected $primaryKey = 'product_no';

    //Fill the model with the data from the user inputs

    protected $fillable=[

    	     'product_no', 
    	     'stock',
             'product',
             'bar_no'
    	     	     
    ];
    public function bar()
    {
        return $this->belongsTo('App\Models\Admin\Stocks\Bars\bar');
    }

}
