<?php

namespace App\Models\Admin\Gadgets;

use Illuminate\Database\Eloquent\Model;

class gadget extends Model
{
     //Define the gadget table
    protected $table='gadget';

    //Set the primary key for a column

    protected $primaryKey = 'gadget_code';

    //Fill the model with the data from the user inputs

    protected $fillable=[

    	     'gadget_code', 
    	     'name',
    	     'description',
    	     'code'	     
    ];
}
