<?php

namespace App\Models\Admin\Matatus;

use Illuminate\Database\Eloquent\Model;

class matatu extends Model
{
     //Define the matatu table
    protected $table='matatu';

    //Set the primary key for a column

    protected $primaryKey = 'mat_no';

    //Fill the model with the data from the user inputs

    protected $fillable=[

    	     'mat_no', 
    	     'no_plate',
    	     'passengers',
    	     'mat_routes'	     
    ];
}
