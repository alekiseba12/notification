<?php

namespace App\Models\Admin\Adverts;

use Illuminate\Database\Eloquent\Model;

class advert extends Model
{
    //display the table for advert

    protected $table='advert';

    //Define the primary key column

    protected $primaryKey='advert_No';

    //Fill the model with the  attributes

    protected $fillable=[
    	'advert_No',
    	'advert_name',
    	'advert_period_time',
    	'location',
    	
    ];
    public function client()
    {
        return $this->belongsTo('App\Models\Clients\client');
    }
}
