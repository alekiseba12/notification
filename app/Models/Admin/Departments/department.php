<?php

namespace App\Models\Admin\Departments;

use Illuminate\Database\Eloquent\Model;

class department extends Model
{
    
     //Define the departwent table
    protected $table='department';

    //Set the primary key for a column

    protected $primaryKey = 'department_No';

    //Fill the model with the data from the user inputs

    protected $fillable=[

    	     'department_No', 
    	     'department_Name',
    	     	     
    ];
}
