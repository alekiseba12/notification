<?php

namespace App\Models\Admin\Clients;

use Illuminate\Database\Eloquent\Model;

class client extends Model
{
    //display the table for client

    protected $table='client';

    //Define the primary key column

    protected $primaryKey='client_No';

    //Fill the model with the  attributes

    protected $fillable=[
    	'client_No',
    	'company',
    	'email',
    	'phone',
    	'location',
    	'address',
    	'percentage',
    ];
    public function adverts()
    {
        return $this->hasMany('App\Models\Admin\Adverts\advert');
    }
}
