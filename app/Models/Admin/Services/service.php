<?php

namespace App\Models\Admin\Services;

use Illuminate\Database\Eloquent\Model;

class service extends Model
{
      //Define the service table
    protected $table='service';

    //Set the primary key for a column

    protected $primaryKey = 'service_No';

    //Fill the model with the data from the user inputs

    protected $fillable=[

    	     'service_No', 
    	     'service',
    	     'description',
    	     'employee_Number'	     
    ];
}
