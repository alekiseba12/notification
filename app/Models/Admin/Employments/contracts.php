<?php

namespace App\Models\Admin\Employments;

use Illuminate\Database\Eloquent\Model;

class contracts extends Model
{
    //Define the departwent table
    protected $table='contracts';

    //Set the primary key for a column

    protected $primaryKey = 'employment_No';

    //Fill the model with the data from the user inputs

    protected $fillable=[

    	     'employment_No', 
    	     'document',
    	     'status',
    	     'user_id',
    	     'status',
             'contract',

    	     	     
    ];
}
