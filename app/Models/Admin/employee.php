<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class employee extends Model
{
     use Notifiable;
    //Define the employee table
    protected $table='employee';

    //Set the primary key for a column

    protected $primaryKey = 'employee_Number';

    //Fill the model with the data from the user inputs

    protected $fillable=[

    	     'employee_Number', 
    	     'fullnames',
    	     'gender',
    	     'email',
    	     'phone',
    	     'address',
    	     'location',
    	     'national_id',
             'pic',
             'salary',
             'type',
             'role_no',
             'department_no'
    ];
    public function user()
    {
        return $this->belongsTo('App\user','id','user_id');
    }
}
