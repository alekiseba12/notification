<?php

namespace App\Models\Admin\Tasks;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class task extends Model
{
     use Notifiable;
      //display the table for task

    protected $table='task';

    //Define the primary key column

    protected $primaryKey='id';

    //Fill the model with the  attributes

    protected $fillable=[
    	'id',
    	'task_name',
    	'start_date',
    	'end_date',
    	'user_id',
    	'description',
        'status'
    	
    ];
    
}
