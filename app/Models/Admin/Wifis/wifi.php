<?php

namespace App\Models\Admin\Wifis;

use Illuminate\Database\Eloquent\Model;

class wifi extends Model
{
    //display the table for wifis

    protected $table='wifi';

    //Define the primary key column

    protected $primaryKey='wifi_code';

    //Fill the model with the  attributes

    protected $fillable=[
    	'wifi-code',
    	'ip_address',
    	'location',
    	
    ];
}
