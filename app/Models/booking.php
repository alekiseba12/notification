<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class booking extends Model
{
     protected $table = 'bookings';

     protected $fillable = [
        "company",
        "email",
        "address",
        "postcode",
        "location",
        "phone",
        "service",
    ];
}
