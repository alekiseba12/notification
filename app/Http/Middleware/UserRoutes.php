<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class UserRoutes
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->User()->role==0) {
            return redirect('/home');
        }
        return $next($request);
    }
}
