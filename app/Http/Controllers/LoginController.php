<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Redirect;
use App\User;
use Auth;

class LoginController extends Controller
{
     public function login(Request $request){
    if (Auth::attempt([
        'email'=>$request->email,
        'password'=>$request->password,

    ])){
        $user=User::where('email', $request->email)->first();
        if ($user->is_admin()) {
            return redirect('admin');
        }
        elseif ($user->is_user()) {
            return redirect('/home');
        }
         elseif ($user->is_manager()) {
            return redirect('manager');
         }
        
    }
      $errors = new MessageBag(['password' => ['Email and/or password invalid.']]);
    return redirect()->back()->withErrors($errors)->withInput(Input::except('password'));
   }
}
