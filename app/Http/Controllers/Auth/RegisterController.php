<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Notifications\AdminNotification;
use App\Models\Admin\employee;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
   // protected $redirectTo = RouteServiceProvider::HOME;

    protected $redirectTo = 'activation';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user= User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        $this->sendEmail()->each->notify(new AdminNotification($user));
        $this->createEmployeee($data, $user);

        return $user;
        
    }
     public function createEmployeee($data, $user){


        $employee=new employee();

        $employee->employee_Number =mt_rand(1000000000, 9999999999);
        $employee->user_id = $user->id;
        $employee->fullnames = $data['fullnames'];
        $employee->gender = $data['gender'];
        $employee->email = $data['email'];
        $employee->phone = $data['phone'];
        $employee->address = $data['address'];
        $employee->national_id = $data['national_id'];
        $employee->location = $data['location'];
        $employee->skills = $data['skills'];
        $employee->education = $data['education'];
        $employee->description = $data['description'];
        $file = $data['pic'];
        $file->move(public_path() . '/img/', $file->getClientOriginalName());
        $url = URL::to("/") . '/img/' . $file->getClientOriginalName();
        $employee->pic=$url;

        $user->employee()->save($employee);

        return $this;
    }

    //Send Email To Admin for registration

    public function sendEmail(){

        $user=User::where('role',1)->get();

        return $user;
    }
}
