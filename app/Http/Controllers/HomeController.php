<?php


namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Notifications\UserNotfication;
use App\Notifications\AdminNotification;
use App\Notifications\SmsNotification;
use App\Models\Admin\employee;
use App\Traits\userTrait;
use App\User;
use App\bookings;
use Auth;
use Validator;


class HomeController extends Controller
{
    use userTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       $user=$this->getLoggedUser();
       $department=$this->department();    
       $task=$this->employeeTasks();
       $contract=$this->contracts();
       return view('Pages.User.index',compact('user','department','task','contract'));
    }

    //to select the user whose is admin tha is aunthiticated

    public function getAdmin(){
     $user=User::where('role',1)->get();
     return $user;
    }

    

    //to create the new booking using Models\booking
    /* public function create(Request $request){
        $user=Auth::User();

        $validate=Validator::make($request->all(),[
                      'company'=>'required|max:200',
                      'address'=>'required',
                      'phone'=>'required|max:15',
                      'email'=>'required|email',
                      'service'=>'required|string',
                      'postcode'=>'required|string',  
                      'location'=>'required|string',    
        ]);
                if ($validate->fails()) {

                return back()->withErrors($validate)->withInput();
        }
        $booking=new bookings();
        $booking->user_id=Auth::user()->id;
        $booking->company=$request->input('company');
        $booking->address=$request->input('address');
        $booking->phone=$request->input('phone');
        $booking->email=$request->input('email');
        $booking->service=$request->input('service');
        $booking->postcode=$request->input('postcode');
        $booking->location=$request->input('location');
        $booking->save();
        $user->notify(new UserNotfication($booking));
        $this->getAdmin()->each->notify(new AdminNotification($booking));
        return redirect('/home')->with('success', 'Booked the service successfully');
    }

    public function isAdmin(){
       $booking=$this->getBookings();

       return view('Pages.Admin.admin',compact('booking'));
    }
    */
  public function activate(){

    $users=employee::where('status','=',0)->get();

       return view('Pages.User.start',compact('users'));   
  }

}
