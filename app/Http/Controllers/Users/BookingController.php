<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\BookingTrait;

class BookingController extends Controller
{
	//Use the trait to call the function for model
	use BookingTrait;

	//Craete the method to output the bookings for
    public function services(){

       $userBookings=$this->userBooks();
       	 return view('Pages.User.bookings',compact('userBookings'));

    }

    //function list(){}
}
