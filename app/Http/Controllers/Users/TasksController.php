<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Tasks\task;

class TasksController extends Controller
{
    //complete the task

    public function edit(Request $request,$id){

            $task=task::where('id',$id)->get()->first();
	        $task->status=$request->input('status');
            $task->update();

            return redirect()->back();
}

     //reason why  the task not done

      public function reason(Request $request,$id){

            $task=task::where('id',$id)->get()->first();
	        $task->reason=$request->input('reason');
            $task->update();

             return redirect()->back();

    }


     //response for  the task 

      public function response(Request $request,$id){

            $task=task::where('id',$id)->get()->first();
	        $task->response=$request->input('response');
            $task->update();

             return redirect()->back();

    }


}
