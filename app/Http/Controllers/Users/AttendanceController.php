<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User\Attendance\attendance;
use App\User;
use Validator;
use Auth;


class AttendanceController extends Controller
{
    //create new attendance
    public function create(Request $request){

    	$validate=Validator::make($request->all(),[
          'attendance_date'=> 'required',
          'attendance_day'=> 'required',
          'time_in'=> 'required',
          'time_out'=> 'required',
         
             ]);
        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput();
        }
        $user=Auth::User()->id;

        $attendance=new attendance();
        $attendance->attendance_date=$request->input('attendance_date');
        $attendance->attendance_day=$request->input('attendance_day');
        $attendance->time_in=$request->input('time_out');
        $attendance->time_out=$request->input('time_in');
        $attendance->user_id=$user;
        $attendance->save();

        return redirect('submit')->with('success','Successfully created advert!');
    }


    }

