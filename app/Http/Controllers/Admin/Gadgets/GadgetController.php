<?php

namespace App\Http\Controllers\Admin\Gadgets;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Gadgets\gadget;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use App\Traits\gadgetTrait;
use Validator;

class GadgetController extends Controller
{
    use gadgetTrait;
    
    //Display the form for creating new service

    public function index(){

        $gadgets=$this->allGadget();
    	return view('Pages.Admin.Gadgets.addGadget',compact('gadgets'));
    }

    //Create new service into the model

    public function create(Request $request){

    	$validate=Validator::make($request->all(),[
          'name'=> 'required',
          'description'=> 'required',
          'code'=> 'required|unique:gadget',
          'photo'=> 'required|file|mimes:peg,png,jpeg',
          
             ]);
        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput();
        }

        $gadget=new gadget();

        $gadget->gadget_code        =mt_rand(1000000000, 9999999999);
        $gadget->name=$request->input('name');
        $gadget->description=$request->input('description');
        $gadget->code=$request->input('code');
        $file = $request['photo'];
        $file->move(public_path() . '/gadgets/', $file->getClientOriginalName());
        $url = URL::to("/") . '/gadgets/' . $file->getClientOriginalName();
        $gadget->photo=$url;
        
        $gadget->save();

        return redirect('new-gadget')->with('success','Successfully created gadget!');
    }

    //Update the Gadget

    public function update(Request $request, $gadget_code){

        $gadget=gadget::where('gadget_code',$gadget_code)->get()->first();

        $gadget->name=$request->input('name');
        $gadget->description=$request->input('description');
        $gadget->code=$request->input('code');
        
        $gadget->update();

        return redirect('new-gadget')->with('success','Successfully updated gadget!');
    }

    //Delete the Gadget

    public function delete($gadget_code){

        $gadget=gadget::where('gadget_code',$gadget_code)->delete();
        
        return redirect('new-gadget')->with('success','Successfully deleted gadget!');

    }
}
