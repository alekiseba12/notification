<?php

namespace App\Http\Controllers\Admin\Roles;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Roles\role;
use App\Traits\roleTrait;
use Validator;

class RoleController extends Controller
{
    //
    use roleTrait;
    
    //Display the form for creating new department

    public function index(){

       $roles=$this->allRoles();

    	return view('Pages.Admin.Roles.addRole', compact('roles'));
    }

    //Create new service into the model


    public function create(Request $request){
    	   $validate=Validator::make($request->all(),[
          'role'=> 'required|unique:role',
             ]);
        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput();
        }

        $role=new role();

        $role->role_No        =mt_rand(1000000000, 9999999999);
        $role->role=$request->input('role');
        $role->save();



        return redirect('new-role')->with('success','Successfully created role');
    }
    //Update the role 

    public function update(Request $request, $role_no){

        $roles= role::where('role_no',$role_no)->get()->first();
        $roles->role=$request->input('role');
        $roles->status=$request->input('status');
        $roles->update();

        return redirect('new-role')->with('success','Successfully updated the role!');
    }

    //Delete the role

    public function delete($role_no){

        $roles=role::where('role_no',$role_no)->delete();

        return redirect('new-role')->with('success','Successfully deleted the role');
    }
}
