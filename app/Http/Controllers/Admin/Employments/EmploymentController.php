<?php

namespace App\Http\Controllers\Admin\Employments;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Notifications\userContractNotification;
use App\Models\Admin\Employments\contracts;
use App\Traits\employmentTrait;
use App\Traits\employeeTrait;
use App\Models\Admin\employee;
use App\User;

class EmploymentController extends Controller
{

    use employmentTrait;
    use employeeTrait;
    
    //Display the form for creating new contract

    public function index(){

        $users=User::all();
        $employments=$this->allEmployments();
        $user=$this->getLoggedinEmployee();
    	return view('Pages.Admin.Employment.addEmployment', compact('employments','user','users'));
    }

    //Create new contract into the model


    public function create(Request $request){
    	   $validate=Validator::make($request->all(),[
          'document'=> 'required|file|mimes:pdf,docs,docx','doc',
          'user_id'=> 'required',
          'contract'=> 'required',
             ]);
        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput();
        }

        $contract=new contracts();

        $contract->employment_No        =mt_rand(1000000000, 9999999999);
        $contract->user_id=$request->input('user_id');
        $contract->contract=$request->input('contract');
        $file = $request['document'];
        $file->move(public_path() . '/documents/', $file->getClientOriginalName());
        $url = URL::to("/") . '/documents/' . $file->getClientOriginalName();
        $contract->document=$url;
        $contract->save();

        $this->sendContractEmail()->notify(new userContractNotification($contract));

        return redirect('new-contract')->with('success','Successfully created contract');
    }

    //Update Contract

    public function update(Request $request, $employment_No){

        $employment=contracts::where('employment_No',$employment_No)->get()->first();
        $employment->user_id=$request->input('user_id');
        $employment->status=$request->input('status');
        $employment->contract=$request->input('contract');
        $employment->update();

        return redirect('new-contract')->with('success','Successfully updated contract');

    }

    //Delete Contract

    public function delete($employment_No){
              
        $employment=contracts::where('employment_No',$employment_No)->delete();

         return redirect('new-contract')->with('success','Successfully deleted contract');

    }
    //send the email to employee

    public function sendContractEmail(){

      $emailSend=employee::select('email')->first();

              return $emailSend;
    }

}
