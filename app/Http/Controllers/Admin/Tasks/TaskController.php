<?php

namespace App\Http\Controllers\Admin\Tasks;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Notifications\userTaskNotification;
use Validator;
use App\Models\Admin\Tasks\task;
use App\Models\Admin\employee;
use App\Traits\taskTrait;
use App\Traits\employeeTrait;
use App\User;
use DB;

class TaskController extends Controller
{
   use taskTrait;
   use employeeTrait;

   //Get the for to create the form new task

    public function index(){
     $users=$this->getAllUsers();
     $tasks=$this->allTasks();
     $allCompanyTasks=$this->getTasks();
     $user=$this->getLoggedinEmployee();


      return view('Pages.Admin.Tasks.addTask',compact('users','tasks','user','allCompanyTasks'));

    }

    //Create new task into the model

    public function create(Request $request){

    	$validate=Validator::make($request->all(),[
          'task_name'=> 'required',
          'start_date'=> 'required',
          'end_date'=> 'required',
          'description'=> 'required',
          'user_id'=> 'required',
         
             ]);
        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput();
        }

        $task=new task();
        $task->task_name=$request->input('task_name');
        $task->start_date=$request->input('start_date');
        $task->end_date=$request->input('end_date');
        $task->description=$request->input('description');
        $task->user_id=$request->input('user_id');
        $task->save();

        $this->sendTaskEmail()->notify(new userTaskNotification($task));

        return redirect('new-task')->with('success','Successfully created task!');
    }

    //Update the task

    public function update(Request $request,$id){

            $task=task::where('id',$id)->get()->first();

          $task->task_name=$request->input('task_name');
	        $task->start_date=$request->input('start_date');
	        $task->end_date=$request->input('end_date');
	        $task->description=$request->input('description');
	        $task->user_id=$request->input('user_id');
	        $task->status=$request->input('status');
          $task->update();

          return redirect('new-task')->with('success','Successfully updated task!');
    }

    //Destroy the task

    public function delete($id){

           task::where('id',$id)->delete();
            
            return redirect('new-task')->with('success','Successfully deleted task!');


    }
    public function sendTaskEmail(){

      $emailSend=employee::select('email')->first();

              return $emailSend;
    }



}
