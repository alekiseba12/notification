<?php

namespace App\Http\Controllers\Admin\Clients;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ClientTrait;
use App\Traits\ServiceTrait;



class ClientsController extends Controller
{
	use ClientTrait;
	use ServiceTrait;
    //Get all the company clients

    public function clients(){

    	$clients=$this->allClients();
    	$countClients=$this->getClients();
    	$services=$this->allServices();
    	$actives=$this->clientsActive();
    	$inactives=$this->clientsInactive();
    	
   return view('Pages.Admin.Clients.viewClients',compact('clients','countClients','services','actives','inactives'));

    }
}
