<?php

namespace App\Http\Controllers\Admin\Clients;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Admin\Clients\client;
use App\Models\Admin\Services\service;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class ClientController extends Controller
{
    //Get the for to create the form new client

    public function index(){
      $services=service::all();

      return view('Pages.Admin.Clients.addClient',compact('services'));

    }

    //Create new client into the model

    public function create(Request $request){

    	$validate=Validator::make($request->all(),[
          'company'=> 'required',
          'email'=> 'required|email|unique:client',
          'phone'=> 'required|unique:client',
          'location'=> 'required',
          'address'=> 'required',
          'percentage'=> 'required',
          'pic'=> 'required|file|mimes:peg,png,jpeg',
             ]);
        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput();
        }

        $client=new client();

        $client->client_No        =mt_rand(1000000000, 9999999999);
        $client->company=$request->input('company');
        $client->email=$request->input('email');
        $client->phone=$request->input('phone');
        $client->address=$request->input('address');
        $client->location=$request->input('location');
        $client->percentage=$request->input('percentage');
        $file = $request['pic'];
        $file->move(public_path() . '/clients/', $file->getClientOriginalName());
        $url = URL::to("/") . '/clients/' . $file->getClientOriginalName();
        $client->pic=$url;

        $client->save();

        return redirect('view-clients')->with('success','Successfully registered client!');
    }

    //update the client

    public function update(Request $request, $client_No){

            $client= client::where('client_No',$client_No)->get()->first();
            $client->company=$request->input('company');
            $client->email=$request->input('email');
            $client->phone=$request->input('phone');
            $client->address=$request->input('address');
            $client->location=$request->input('location');
             $client->status=$request->input('status');
            $client->percentage=$request->input('percentage');

            $client->update();

            return redirect('view-clients')->with('success','Successfully Updated client!');
    }

    //Delete the client

    public function delete($client_No){

          $client=client::where('client_No',$client_No)->delete();

          return redirect('view-clients')->with('success','Successfully Deleted client!');


    }
}
