<?php

namespace App\Http\Controllers\Admin\Wifis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Wifis\wifi;
use Validator;

class WifiController extends Controller
{
   //Get the for to create the form new wifi

    public function index(){

      return view('Pages.Admin.Wifis.addWifi');

    }

    //Create new client into the model

    public function create(Request $request){

    	$validate=Validator::make($request->all(),[
          'ip_address'=> 'required',
          'location'=> 'required',
             ]);
        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput();
        }

        $wifi=new wifi();

        $wifi->wifi_code        =mt_rand(1000000000, 9999999999);
        $wifi->ip_address=$request->input('ip_address');
        $wifi->location=$request->input('location');

        $wifi->save();

        return redirect('new-wifi')->with('success','Successfully created wifi!');
    }
}
