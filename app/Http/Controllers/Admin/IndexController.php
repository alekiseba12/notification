<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\EmployeeTrait;
use App\Traits\ClientTrait;
use App\Traits\ServiceTrait;
use App\Traits\departmentTrait;
use App\Traits\gadgetTrait;
use App\Traits\matatuTrait;
use App\Traits\advertTrait;
use App\Traits\wifiTrait;
use App\Traits\roleTrait;


class IndexController extends Controller
{
	//Use trait to display the all employees

	use EmployeeTrait;
	use ClientTrait;
	use ServiceTrait;
  use departmentTrait;
  use gadgetTrait;
  use matatuTrait;
  use advertTrait;
  use wifiTrait;
  use roleTrait;
    //Show admin dashboard

    public function isAdmin(){
       
       $employees=$this->getAllEmployees();
       $allClients=$this->allClients();
       $clients=$this->getClients();
       $services=$this->getServices();
       $departments=$this->getDepartments();
       $gadgets=$this->getGadgets();
       $matatus=$this->getMatatus();
       $adverts=$this->getAdverts();
       $wifi=$this->getWifi();
       $roles=$this->getRoles();
       $activeStatus=$this->countsActive();
       $inactiveStatus=$this->countsInactive();
       $completedStatus=$this->countsCompleted();
       $declinedStatus=$this->countsDeclined();
       $managements=$this->managementTeam();
       $companyServices=$this->allServices();
       $user=$this->getLoggedinEmployee();

        return view('Pages.Admin.admin',compact('employees', 'clients', 'services','departments','gadgets','matatus','wifi','adverts','roles','allClients','activeStatus','inactiveStatus','completedStatus','declinedStatus', 'managements','companyServices','user'));
    }

    //get User contacts

    public function getContacts(){

      $contacts=$this->managementTeam();
      $user=$this->getLoggedinEmployee();

      return view('Pages.Admin.contacts', compact('contacts','user'));
    }

    
}
