<?php

namespace App\Http\Controllers\Admin\Stock\Bars;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Stocks\Bars\bar;
use App\Traits\Stock\barTrait;
use Validator;

class BarController extends Controller
{
    //use stock\barTrait to get specific products for a bar
      use barTrait;

    //create new Bar
    public function create(Request $request){

    	  $validate=Validator::make($request->all(),[
          'bar_name'=> 'required', 
          'region_no'=> 'required',  
             ]);
        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput();
        }

        $bar=new bar();

        $bar->bar_no        =mt_rand(1000000000, 9999999999);
        $bar->bar_name=$request->input('bar_name');
        $bar->region_no=$request->input('region_no');     
        $bar->save();

        return redirect('nyce_bar_stock_exchange')->with('success','Successfully created bar!');
    }

    //To view the products that belongs to each bar

       public function viewProduct($bar_no){

        $barProducts=$this->getBar($bar_no);

        return view('Pages.Admin.Stock.Bar.products',compact('barProducts'));
       }



  
}
