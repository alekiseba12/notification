<?php

namespace App\Http\Controllers\Admin\Stock\Bars;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\Stock\barTrait;

class BarsController extends Controller
{
	//use stock/bar trait to display the data
    //get All Bars
    use barTrait;

    public function index(){
  
        $bars=$this->getAllBars();
         
         return view('Pages.Admin.Stock.bars',compact('bars'));

    }
}
