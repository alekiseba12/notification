<?php

namespace App\Http\Controllers\Admin\Stock;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\stockTrait;
use Validator;

class StockController extends Controller
{


    
    //Use the trait to show the counties
    use stockTrait;

    //use serviceTrait;
    //Display the form for creating new service

    public function index(){
         $counties=$this->allCounties();
         $regions=$this->allRegions();
         $bars=$this->allBars();
         $products=$this->allProducts();

         $limitCounties=$this->limitCounties();
         $limitRegions=$this->limitRegions();
         $limitBars=$this->limitBars();
         $limitProducts=$this->limitProducts();

    	return view('Pages.Admin.Stock.addStock',compact('counties','regions','bars','products','limitProducts','limitBars', 'limitRegions','limitCounties'));
    }

}
