<?php

namespace App\Http\Controllers\Admin\Stock\Regions;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\Stock\regionsTrait;

class RegionsController extends Controller
{
   
	//use stock/region trait to display the data
    //get All regions
    use regionsTrait;

    public function index(){
  
        $regions=$this->getAllRegions();
         
         return view('Pages.Admin.Stock.regions',compact('regions'));

    }
}
