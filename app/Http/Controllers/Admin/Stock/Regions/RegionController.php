<?php

namespace App\Http\Controllers\Admin\Stock\Regions;

use App\Http\Controllers\Controller;
use App\Models\Admin\Stocks\Regions\region;
use Illuminate\Http\Request;
use App\Traits\Stock\regionsTrait;
use Validator;

class RegionController extends Controller
{
     use regionsTrait;
    //create new Region
    public function create(Request $request){

    	  $validate=Validator::make($request->all(),[
          'region_name'=> 'required|unique:region', 
          'county_no'=> 'required',  
             ]);
        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput();
        }

        $region=new region();

        $region->region_no        =mt_rand(1000000000, 9999999999);
        $region->region_name=$request->input('region_name');
        $region->county_no=$request->input('county_no');     
        $region->save();

        return redirect('nyce_bar_stock_exchange')->with('success','Successfully created region!');
    }

    //To view the bars that belongs to each region

       public function viewBars($region_no){

        $regionsBars=$this->getRegion($region_no);

        return view('Pages.Admin.Stock.Region.bars',compact('regionsBars'));
       }

}
