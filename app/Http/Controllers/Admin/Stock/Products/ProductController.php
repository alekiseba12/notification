<?php

namespace App\Http\Controllers\Admin\Stock\Products;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Stocks\Products\product;
use Validator;

class ProductController extends Controller
{
    
    //create new Product
    public function create(Request $request){

    	  $validate=Validator::make($request->all(),[
          'product'=> 'required', 
          'stock'=>'required|numeric',
          'bar_no'=> 'required', 
             ]);
        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput();
        }
   

        $product=new product();

        $product->product_no        =mt_rand(1000000000, 9999999999);
        $product->product=$request->input('product');
        $product->stock=$request->input('stock');   
        $product->bar_no=$request->input('bar_no');    
        $product->save();

        return redirect('nyce_bar_stock_exchange')->with('success','Successfully created product!');
    }
}
