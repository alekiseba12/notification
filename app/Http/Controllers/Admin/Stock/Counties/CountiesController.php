<?php

namespace App\Http\Controllers\Admin\Stock\Counties;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\Stock\countyTrait;

class CountiesController extends Controller
{
    
	//use stock/region trait to display the data
    //get All regions
    use countyTrait;

    public function index(){
  
        $counties=$this->getAllCounties();
         
         return view('Pages.Admin.Stock.counties',compact('counties'));

    }
}
