<?php

namespace App\Http\Controllers\Admin\Stock\Counties;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Stocks\Counties\county;
use App\Traits\Stock\countyTrait;
use Validator;

class CountyController extends Controller
{ 
     use countyTrait;
    //create new county
    public function create(Request $request){

    	  $validate=Validator::make($request->all(),[
          'county'=> 'required|unique:county',   
             ]);
        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput();
        }

        $county=new county();

        $county->county_No        =mt_rand(1000000000, 9999999999);
        $county->county=$request->input('county');   
        $county->save();

        return redirect('nyce_bar_stock_exchange')->with('success','Successfully created county!');
    }

    //To view the regions that belongs to each county

       public function viewRegions($county_no){

        $countyRegions=$this->getCounty($county_no);

        return view('Pages.Admin.Stock.Countie.regions',compact('countyRegions'));
       }
}
