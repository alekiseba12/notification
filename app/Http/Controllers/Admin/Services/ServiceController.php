<?php

namespace App\Http\Controllers\Admin\Services;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Services\Service;
use App\Models\Admin\employee;
use App\Traits\serviceTrait;
use App\Traits\EmployeeTrait;
use Validator;

class ServiceController extends Controller
{
    //Use the trait to show the services

    use serviceTrait;

    use EmployeeTrait;
    //Display the form for creating new service

    public function index(){
        $services=$this->allServices();
        $employees=$this->allEmployees();


    	return view('Pages.Admin.Services.addService',compact('services','employees'));
    }

    //Create new service into the model

    public function create(Request $request){

    	$validate=Validator::make($request->all(),[
          'service'=> 'required',
          'description'=> 'required|unique:service',
          'employee_Number'=> 'required|unique:service',
          
             ]);
        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput();
        }

        $service=new service();

        $service->service_No        =mt_rand(1000000000, 9999999999);
        $service->service=$request->input('service');
        $service->description=$request->input('description');
        $service->employee_Number=$request->input('employee_Number');
        
        $service->save();

        return redirect('new-service')->with('success','Successfully created service!');
    }
    //update the service

    public function update(Request $request,$service_No){

        $service=service::where('service_No',$service_No)->get()->first();
        $service->service=$request->input('service');
        $service->description=$request->input('description');
        $service->employee_Number=$request->input('employee_Number');
        
        $service->update();

        return redirect('new-service')->with('success','Successfully updated service!');

    }

    //Delete the service

    public function delete($service_No){

        $service=service::where('service_No',$service_No)->delete();

         return redirect('new-service')->with('success','Successfully deleted service!');


    }
}
