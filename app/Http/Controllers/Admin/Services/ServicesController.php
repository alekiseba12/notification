<?php

namespace App\Http\Controllers\Admin\Services;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits/serviceTrait;

class ServicesController extends Controller
{
   //Use the trait to show the services

	use serviceTrait;

    //Display all the employees

    public function index(){

    	$services=$this->allServices();

        return view('Pages.Admin.Services.forms.create',compact('services'));

    }
}
