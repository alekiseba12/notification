<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\EmployeeTrait;
use App\Models\Admin\employee;

class EmployeesController extends Controller
{
	//Use the trait to show the employees registered

	use EmployeeTrait;

    //Display all the employees

    public function index(){

    	$employees=$this->managementTeam();

        return view('Pages.Admin.Employees.viewEmployees',compact('employees'));

    }
    function list(){
    	return employee::all();
    }
}
