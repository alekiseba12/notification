<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Models\Admin\employee;
use App\Models\Admin\Roles\role;
use App\Traits\departmentTrait;
use App\Traits\roleTrait;
use App\User;
use DB;
use App\Models\Admin\Departments\department;
use App\Notifications\ProfileNotification;
use Validator;

class EmployeeController extends Controller

{

    use roleTrait;
    //Display the form for creating new employee
    public function index(){
      $departments=department::all();
      $roles=$this->allRoles();
      $users=User::all();
      return view('Pages.Admin.Employees.addEmployee',compact('departments','roles','users'));

    }
    //Create new employee into the model

    public function create(Request $request){

    	$validate=Validator::make($request->all(),[
          'fullnames'=> 'required',
          'gender'=> 'required',
          'phone'=> 'required|unique:employee',
          'email'=> 'required|email|unique:employee',
          'location'=> 'required',
          'address'=> 'required',
          'national_id'=> 'required|max:8|unique:employee',
          'pic'=> 'required|file|mimes:peg,png,jpeg',
          'salary'=> 'required|max:300,000',
          'type'=> 'required',
          'role_no'=>'required',
          'department_No'=>'required',
          'user_id'=>'required|unique:employee',
          'department_No'=>'required',
          'skills'=>'required',
          'description'=>'required',
          'education'=>'required',

             ]);
        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput();
        }

        $employee=new employee();

        $employee->employee_Number        =mt_rand(1000000000, 9999999999);
        $employee->fullnames=$request->input('fullnames');
        $employee->email=$request->input('email');
        $employee->gender=$request->input('gender');
        $employee->phone=$request->input('phone');
        $employee->address=$request->input('address');
        $employee->location=$request->input('location');
        $employee->national_id=$request->input('national_id');
        $file = $request['pic'];
        $file->move(public_path() . '/img/', $file->getClientOriginalName());
        $url = URL::to("/") . '/img/' . $file->getClientOriginalName();
        $employee->pic=$url;
        $employee->salary=$request->input('salary');
        $employee->type=$request->input('type');
        $employee->role_no=$request->input('role_no');
        $employee->department_No=$request->input('department_No');
        $employee->user_id=$request->input('user_id');
        $employee->education=$request->input('education');
        $employee->skills=$request->input('skills');
        $employee->description=$request->input('description');
        $employee->save();

        return redirect('employee-registration-form')->with('success','Successfully registered an Employee!');
    }


    public function show($employee_Number){
      $employeeDetails=DB::table('employee')
                      //->join('users','users.id','=','employee.user_id')
                      ->join('department', 'department.department_No','=','employee.department_No')
                      ->join('role', 'role.role_no','=','employee.role_no')
                      ->select('employee.*','department.*', 'role.*')
                      ->where('employee.employee_Number',$employee_Number)
                      ->get();

             return view('Pages.Admin.Employees.viewEmployee',compact('employeeDetails'));
    }
    //Dispaly the form to edit the emoployee info

    public function edit($employee_Number){

                $employee=employee::find($employee_Number);

                $roles=role::all();
                $role=role::find($employee->role_no);
                $users=user::all();
                $user=user::find($employee->user_id);
                $departments=department::all();
                $department=department::find($employee->department_No);

                return view('Pages.Admin.Employees.editEmployee',compact('department','departments','employee','roles','role','users','user'));

    }
    //Update the employee details
    public function update(Request $request,$employee_Number){
        $employee= employee::where('employee_Number',$employee_Number)->get()->first();
        $employee->fullnames=$request->input('fullnames');
        $employee->gender=$request->input('gender');
        $employee->email=$request->input('email');
        $employee->phone=$request->input('phone');
        $employee->address=$request->input('address');
        $employee->location=$request->input('location');
        $employee->national_id=$request->input('national_id');
        $employee->salary=$request->input('salary');
        $employee->type=$request->input('type');
        $employee->role_no=$request->input('role_no');
        $employee->department_No=$request->input('department_No');
        $employee->user_id=$request->input('user_id');
        $employee->education=$request->input('education');
        $employee->skills=$request->input('skills');
        $employee->status=$request->input('status');
        $employee->update();

        $employee->notify(new ProfileNotification());

        return redirect('management-team')->with('success', 'Successfully updated Employee Details');
    }

    //Delete the employee

    public function delete($employee_Number){

        $employee=employee::where('employee_Number',$employee_Number)->delete();

        return redirect('management-team')->with('success', 'Successfully deleted an Employee');
      }

       
    
}
