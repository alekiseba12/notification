<?php

namespace App\Http\Controllers\Admin\Matatus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Matatus\matatu;
use App\Traits\matatuTrait;
use Validator;

class MatatuController extends Controller
{
    use matatuTrait;
    //Display the form for creating new matatu

    public function index(){

      $matatus=$this->allMatatus();

    	return view('Pages.Admin.Matatus.addMatatu',compact('matatus'));
    }

    //Create new matatu into the model

    public function create(Request $request){

    	$validate=Validator::make($request->all(),[
          'mat_routes'=> 'required',
          'passengers'=> 'required|max:50',
          'name'=> 'required',
          'no_plate'=> 'required|max:8',
          
             ]);
        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput();
        }

        $matatu=new  matatu();

        $matatu->mat_no        =mt_rand(1000000000, 9999999999);
        $matatu->no_plate=$request->input('no_plate');
        $matatu->mat_routes=$request->input('mat_routes');
        $matatu->passengers=$request->input('passengers');
        $matatu->name=$request->input('name');
        
        $matatu->save();

        return redirect('new-matatu')->with('success','Successfully created matatu!');
    }

    //update the matatu

    public function update(Request $request, $mat_no){

        $matatu=matatu::where('mat_no',$mat_no)->get()->first();
        $matatu->no_plate=$request->input('no_plate');
        $matatu->mat_routes=$request->input('mat_routes');
        $matatu->passengers=$request->input('passengers');
        $matatu->name=$request->input('name');
        
        $matatu->update();

        return redirect('new-matatu')->with('success','Successfully updated matatu!');

    }

    //Delete the matatu

    public function delete($mat_no){

         matatu::where('mat_no',$mat_no)->delete();

         return redirect('new-matatu')->with('success','Successfully deleted matatu!');


    }
}
