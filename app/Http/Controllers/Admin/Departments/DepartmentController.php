<?php

namespace App\Http\Controllers\Admin\Departments;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Models\Admin\Departments\department;
use App\Traits\departmentTrait;
use App\Traits\employeeTrait;

class DepartmentController extends Controller
{
    use departmentTrait;
    use employeeTrait;
    
    //Display the form for creating new department

    public function index(){

        $departments=$this->allDepartments();
        $user=$this->getLoggedinEmployee();
    	return view('Pages.Admin.Departments.addDepartment', compact('departments','user'));
    }

    //Create new service into the model


    public function create(Request $request){
    	   $validate=Validator::make($request->all(),[
          'department_Name'=> 'required|unique:department',
          'pic'=> 'required|file|mimes:peg,png,jpeg',
          'description'=> 'required|max:100',
          'employees'=> 'required|max:4',
             ]);
        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput();
        }

        $department=new department();

        $department->department_No        =mt_rand(1000000000, 9999999999);
        $department->department_Name=$request->input('department_Name');
        $department->description=$request->input('description');
        $department->employees=$request->input('employees');
        $file = $request['pic'];
        $file->move(public_path() . '/department/', $file->getClientOriginalName());
        $url = URL::to("/") . '/department/' . $file->getClientOriginalName();
        $department->pic=$url;
        $department->save();



        return redirect('new-department')->with('success','Successfully created department');
    }

    //Update Department

    public function update(Request $request, $department_No){

        $department=department::where('department_No',$department_No)->get()->first();
        $department->department_Name=$request->input('department_Name');
        $department->employees=$request->input('employees');
        $department->description=$request->input('description');
        $department->employees=$request->input('employees');
        $department->status=$request->input('status');
        $department->reason=$request->input('reason');
        $department->percentage=$request->input('percentage');
        $department->update();

        return redirect('new-department')->with('success','Successfully updated department');

    }

    //Delete Department

    public function delete($department_No){
              
        $department=department::where('department_No',$department_No)->delete();

         return redirect('new-department')->with('success','Successfully deleted department');

    }
}
