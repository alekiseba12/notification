<?php

namespace App\Http\Controllers\Admin\Adverts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Admin\Adverts\advert;
use App\Traits\advertTrait;
use App\Traits\clientTrait;

class AdvertController extends Controller
{
  use advertTrait;

  use clientTrait;
   //Get the for to create the form new advert

    public function index(){
     $allAdverts=$this-> getAdverts();

     $adverts=$this->allAdverts();
     $clients=$this->allClients();
     $activeStatus=$this->countsActive();
     $inactiveStatus=$this->countsInactive();
     $completedStatus=$this->countsCompleted();
     $declinedStatus=$this->countsDeclined();

      return view('Pages.Admin.Adverts.addAdvert',compact('allAdverts','adverts','clients','activeStatus','inactiveStatus','completedStatus','declinedStatus'));

    }

    //Create new client into the model

    public function create(Request $request){

    	$validate=Validator::make($request->all(),[
          'advert_name'=> 'required|unique:advert',
          'advert_period_time'=> 'required',
          'location'=> 'required',
          'client_No'=> 'required',
         
             ]);
        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput();
        }

        $advert=new advert();

        $advert->advert_No        =mt_rand(1000000000, 9999999999);
        $advert->advert_name=$request->input('advert_name');
        $advert->advert_period_time=$request->input('advert_period_time');
        $advert->location=$request->input('location');
        $advert->client_No=$request->input('client_No');
        $advert->save();

        return redirect('new-advert')->with('success','Successfully created advert!');
    }

    //Update the advert

    public function update(Request $request,$advert_No){

            $advert=advert::where('advert_No',$advert_No)->get()->first();

            $advert->advert_name=$request->input('advert_name');
            $advert->advert_period_time=$request->input('advert_period_time');
            $advert->location=$request->input('location');
            $advert->client_No=$request->input('client_No');
            $advert->status=$request->input('status');
            $advert->update();

             return redirect('new-advert')->with('success','Successfully updated advert!');
    }

    //Destroy the advert

    public function delete($advert_No){

           advert::where('advert_No',$advert_No)->delete();
            
            return redirect('new-advert')->with('success','Successfully deleted advert!');


    }


}
