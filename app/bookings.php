<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class bookings extends Model
{
     use Notifiable;
     
    protected $table = 'bookings';

     protected $fillable = [
        "company",
        "email",
        "address",
        "postcode",
        "location",
        "phone",
        "service",
    ];
    public function user()
    {
        return $this->belongsTo('App\User','id','user_id');
    }
}
