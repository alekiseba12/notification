<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<br>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

@include('Layouts.title')

  <!-- Font Awesome Icons -->
 @include('Pages.User.Styles.css')
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-collapse layout-top-nav">
<div class="wrapper">
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-8">
          @include('Pages.Admin.Templates.marquee')
          </div><!-- /.col -->
          <div class="col-sm-4">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item" style="color: red; font-size: 48;">Nycemedia Services</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <br><br>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
      <div class="row">
          <!-- /.col-md-6 -->
    <div class="col-lg-12">
    <div class="card card-danger card-outline">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign up</p>

     <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
        @csrf
         <div class="row">
        <div class="col-lg-4">
        <div class="input-group mb-3">
          <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="{{ __('Name') }}" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
          @error('name')
          <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
          </span>
          @enderror 
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        </div>
         <div class="col-lg-8">
        <div class="input-group mb-3">
          <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
          @error('email')
          <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
          </span>
          @enderror 
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
    </div>
    </div>
        <div class="row">
        <div class="col-lg-6">
        <div class="input-group mb-3">
          <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="{{ __('Password') }}" name="password" required autocomplete="current-password">
          @error('password')
          <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
          </span>
          @enderror
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        </div>
        <div class="col-lg-6">
        <div class="input-group mb-3">
        <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="{{ __('Confirm Password') }}" name="password_confirmation" required autocomplete="new-password">
         <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        </div>
        </div>
        <hr>
         <h6 class="mt-4 mb-2">Personal Info</h5>
        <div class="row">
        <div class="col-lg-4">
        <div class="input-group mb-3">
          <input type="text" class="form-control @error('fullnames') is-invalid @enderror" placeholder="{{__('employees.Employee_fullname')}}" name="fullnames" required autocomplete="">
          @error('fullnames')
          <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
          </span>
          @enderror
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        </div>
      <div class="col-lg-4">
        <div class="input-group mb-3">
          <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="{{__('employees.Employee_email')}}" name="email" required autocomplete="">
          @error('email')
          <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
          </span>
          @enderror
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        </div>
        <div class="col-lg-4">
        <div class="input-group mb-3">
          <input type="text" class="form-control @error('gender') is-invalid @enderror" placeholder="{{__('employees.Employee_gender')}}" name="gender" required autocomplete="">
          @error('gender')
          <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
          </span>
          @enderror
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        </div>
        </div>
          <div class="row">
        <div class="col-lg-6">
        <div class="input-group mb-3">
          <input type="text" class="form-control @error('address') is-invalid @enderror" placeholder="{{__('employees.Employee_address')}}" name="address" required autocomplete="">
          @error('address')
          <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
          </span>
          @enderror
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        </div>
      <div class="col-lg-6">
        <div class="input-group mb-3">
          <input type="text" class="form-control @error('location') is-invalid @enderror" placeholder="{{__('employees.Employee_location')}}" name="location" required autocomplete="">
          @error('location')
          <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
          </span>
          @enderror
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        </div>
        </div>

        <div class="row">
        <div class="col-lg-6">
        <div class="input-group mb-3">
          <input type="text" class="form-control @error('national_id') is-invalid @enderror" placeholder="{{__('employees.Employee_id')}}" name="national_id" required autocomplete="">
          @error('national_id')
          <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
          </span>
          @enderror
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        </div>
      <div class="col-lg-6">
        <div class="input-group mb-3">
          <input type="text" class="form-control @error('phone') is-invalid @enderror" placeholder="{{__('employees.Employee_phone')}}" name="phone" required autocomplete="">
          @error('phone')
          <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
          </span>
          @enderror
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-phone"></span>
            </div>
          </div>
        </div>
        </div>
        </div>
               <div class="row">
                <div class="col-lg-6">
                <div class="input-group mb-3">
                  <select class="form-control custom-select" name="education">
                  <option selected disabled>Select {{__('employees.Employee_education')}}</option>
                  <option value="Secondary School">Secondary Schooll</option>
                  <option value="College">College</option>
                  <option value="University">University</option>
                </select>
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-graduation-cap"></i></span>
                  </div>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="input-group">

                  <input type="text" class="form-control" placeholder="{{__('employees.Employee_skill')}}" name="skills">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                </div>
              </div>
            </div>

               <div class="input-group mb-3">
                <div class="input-group">
                <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile" name="pic">
                        <label class="custom-file-label" for="exampleInputFile">{{__('employees.Employee_pic')}}</label>
                      </div>
              </div>
            </div>
            <h5 class="mt-4 mb-2">{{__('employees.Employee_description')}}</h5>
            <div class="mb-3">
            <textarea class="textarea" placeholder="Place some text here" name="description" 
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember" {{ old('remember') ? 'checked' : '' }}>
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-success btn-block">Sign Up</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <div class="social-auth-links text-center mb-3">
        <p>- OR -</p>
        <a href="{{route('login')}}" class="btn btn-block btn-danger">
          <i class="fab fa-thumb-up mr-2"></i> Sign in
        </a>

      </div>
      <!-- /.social-auth-links -->

    </div>
    <!-- /.login-card-body -->
  </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  @include('Pages.Admin.Templates.footer')
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
@include('Pages.Admin.Styles.js')
</body>
</html>
