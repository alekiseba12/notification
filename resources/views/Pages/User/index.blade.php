<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  @include('Pages.User.Templates.title')
  <!-- Font Awesome Icons -->
  @include('Pages.User.Styles.css')

</head>

<body class="hold-transition layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
@include('Pages.User.Templates.nav')
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
 
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
           @if(isset($user->id))
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><small>{{$user->fullnames}}</small></h1>
          </div><!-- /.col -->
      
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">{{$user->department_Name}}</a></li>
              <li class="breadcrumb-item">{{$user->role}}</li>
            </ol>
          </div><!-- /.col -->
          @include('Pages.Admin.Departments.forms.addDepartmentSuccessMessage')
          @include('Pages.Admin.Departments.forms.addDepartmentErrorMessage')
          @endif
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
         <div class="col-md-6">
          @if(empty($user->salary))
          <div class="alert alert-warning">
            {{__('user.User_Alert')}}
            
          </div>
          @endif
            <!-- Widget: user widget style 2 -->
            <div class="card card-widget widget-user-2">
              @if(isset($user->id))
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-warning">
                <div class="widget-user-image">
                  <img class="img-circle elevation-2" src="{{$user->pic}}" alt="User Avatar">
                </div>
                <!-- /.widget-user-image -->
                <h3 class="widget-user-username">{{$user->role}}</h3>
                <h5 class="widget-user-desc">{{$user->skills}}</h5>
              </div>

                <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                      <th>Employee No</th>
                      <th>Gender</th>
                      <th>Status</th>
                      <th>Phone</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td><a href="pages/examples/invoice.html">{{$user->employee_Number}}</a></td>
                      <td>{{$user->gender}}</td>
                      <td>
                        @if($user->status==0)
                        <span class="badge badge-warning">Unverified</span>
                        @elseif($user->status==1)
                        <span class="badge badge-success">Verified</span>
                        @elseif($user->status==2)
                        <span class="badge badge-danger">Deleted</span>
                        @endif
                      </td>
                      <td>
                        <div class="sparkbar" data-color="#00a65a" data-height="20">{{$user->phone}}</div>
                      </td>
                         <td class="project-actions text-right">
                          <a href="javascript:;" class="btn btn-info btn-sm" data-toggle="modal" data-target="#key-{{$user->employee_Number}}">
                   
                              View 
                          </a>
                      </td>
                    </tr>
                   
                    </tbody>
                  </table>
                </div>
            </div>
              @endif
                  <div class="card card-danger">
              <div class="card-header">
                <h5 class="card-title m-0">{{__('tasks.Task_title')}}</h5>
              </div>
              <div class="card-body">
                  <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                      <th>#No</th>
                      <th>Task</th>
                      <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                       @if(isset($task->id))
                    <tr>
                   
                      <td><a href="javascript::">{{$task->id}}</a></td>
                      <td>{{$task->task_name}}</td>
                      <td>
                        @if($task->status==0)
                          <span class="badge badge-warning">Assigned</span>
                          @elseif($task->status==1)
                          <span class="badge badge-success">Completed</span>
                          @elseif($task->status==2)
                          <span class="badge badge-danger">Timed Out</span>
                         
                          @endif
                      </td>

                         <td class="project-actions text-right">
                             @if($task->status==0)
                             <a href="javascript:;" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#key-{{$task->id}}">
                              View 
                          </a>
                          <a href="javascript:;" class="btn btn-info btn-sm" data-toggle="modal" data-target="#edit-{{$task->id}}">
                              Edit
                          </a>
                              
                          </a>
                            @elseif($task->response !=null)
                          <a href="javascript:;" class="btn btn-success btn-sm" data-toggle="modal" data-target="#non">
                              Thank You
                          </a>
                            @elseif($task->status==1)
                            <a href="javascript:;" class="btn btn-success btn-sm" data-toggle="modal" data-target="#response-{{$task->id}}">
                              Response
                          </a>
                           @elseif($task->reason !=null)
                          <a href="javascript:;" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#non">
                              Thank You
                          </a>

                           @elseif($task->status==2)
                             <a href="javascript:;" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#reason-{{$task->id}}">                 
                              Reason 

                              
                          @endif
                      </td>
                    </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
         
          <!-- /.col-md-6 -->
          <div class="col-lg-6">
             @if(isset($department->department_No))
            <div class="card card-warning card-outline">
              <div class="card-header">
                <h5 class="card-title m-0">{{__('user.User_department')}}</h5>
              </div>
             <div class="attachment-block clearfix">
                  <img class="attachment-img" src="{{$department->pic}}" alt="Attachment Image">
                                              
                  <div class="attachment-pushed">
                    <h4 class="attachment-heading"><a href="javascript::"></a>{{$department->department_Name}}</h4>

                    <div class="attachment-text">
                      {{$department->description}}... <a href="#">more</a>
                    </div>
                    <!-- /.attachment-text -->
                  </div>

                  <br>
                    @if($department->status==0)
                  &nbsp; &nbsp;
                <button type="button" class="btn btn-danger btn-sm"><i class="fas fa-home"></i> Inactive</button>
                <button type="button" class="btn btn-warning btn-sm"><i class="fas fa-users"></i> &nbsp; &nbsp;{{$department->employees}}</button>
                <span class="float-right text-muted">{{$department->percentage}} % - {{$department->reason}}</span>
                @elseif($department->status==1)
                <button type="button" class="btn btn-success btn-sm"><i class="fas fa-home"></i> Active</button>
                <button type="button" class="btn btn-warning btn-sm"><i class="fas fa-users"></i> &nbsp; &nbsp;{{$department->employees}}</button>
                <span class="float-right text-muted">{{$department->percentage}} % - {{$department->reason}}</span>
                @elseif($department->status==2)
                 <button type="button" class="btn btn-warning btn-sm"><i class="far fa-home"></i> Fair</button>
                 <button type="button" class="btn btn-warning btn-sm"><i class="fas fa-users"></i> &nbsp; &nbsp;{{$department->employees}}</button>
                <span class="float-right text-muted">{{$department->percentage}} % - {{$department->reason}}</span>
                @elseif($department>status==3)
                 <button type="button" class="btn btn-info btn-sm"><i class="far fa-home"></i> Poor </button>
                 <button type="button" class="btn btn-warning btn-sm"><i class="fas fa-users"></i> &nbsp; &nbsp;{{$department->employees}}</button>
                <span class="float-right text-muted">{{$department->percentage}} % - {{$department->reason}}</span>
                 @endif

 
                  <!-- /.attachment-pushed -->
                </div>

            </div>
              @endif
            <div class="card card-success card-outline">
              <div class="card-header">
                <h5 class="card-title m-0">{{__('user.User_contracts')}}</h5>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                      <th>#No</th>
                      <th>Contract</th>
                      <th>Document</th>
                      <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                       @if(isset($contract->employment_No))
                    <tr>
                   
                      <td><a href="javascript::">{{$contract->employment_No}}</a></td>
                      <td>{{$contract->contract}}</td>
                      <td>
                        @if($contract->status==0)
                       <a href="{{asset($contract->document)}}" download=""><span class="badge bg-success"><i class="fa fa-download"> View</i></span></a>
                       @elseif($contract->status==1)
                       <a href="{{asset($contract->document)}}" download=""><span class="badge bg-danger"><i class="fa fa-download"> Check</i></span></a>
                       @elseif($contract->status==2)
                       <a href="{{asset($contract->document)}}" download=""><span class="badge bg-warning"><i class="fa fa-download"> Check</i></span></a>
                         @elseif($contract->status==3)
                       <a href="{{asset($contract->document)}}" download=""><span class="badge bg-info"><i class="fa fa-download"> Check</i></span></a>
                         @endif
                      </td>
                      <td>
                        @if($contract->status==0)
                        <span class="badge badge-success">New</span>
                        @elseif($contract->status==1)
                         <span class="badge badge-danger">Completed</span>
                         @elseif($contract->status==2)
                         <span class="badge badge-warning">Terminated</span>
                          @elseif($contract->status==3)
                         <span class="badge badge-info">Renewed</span>
                         @endif
                      </td>
                    </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
        
  </div>
  <!-- /.content-wrapper -->

    <!--Modal to view the details-->
        @if(isset($user->id))
      <div class="modal fade" id="key-{{$user->employee_Number}}">
        <div class="modal-dialog modal-lg">  
          <div class="modal-content bg-info">
          <form action="#" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{__('user.User_title')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                  <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                      <th>Address</th>
                      <th>Location</th>
                      <th>National ID</th>
                      <th>Education</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>{{$user->address}}</td>
                      <td>{{$user->location}}</td>
                      <td>
                        {{$user->national_id}}
                        
                      </td>
                      <td>
                        <div class="sparkbar" data-color="#00a65a" data-height="20">{{$user->education}}</div>
                      </td>
                
                    </tr>
                   
                    </tbody>
                  </table>
                </div>
                   <div class="row">
                <div class="col-12">
                    <div class="post">
                      <div class="user-block">
                        <img class="img-circle img-bordered-sm" src="{{$user->pic}}" alt="user image">
                        <span class="username">
                          <a href="#" class="text text-white">{{$user->email}}.</a>
                        </span>
                        <span class="description text-white">Hired Date - {{\Carbon\Carbon::parse($user->created_at)->format('d/m/Y')}}</span>
                        <span class="description text-white">Employment - {{$user->type}}</span>
                        <span class="description text-white">Salary-  Ksh {{$user->salary}}</span>
                        <span class="description text-white">Skills - {{$user->skills}}</span>
                        <hr>
                        <span class="description text-white">About Me - {{$user->description}}</span>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

        <!-----View Tasks  --->
        @if(isset($task->id))
        <div class="modal fade" id="key-{{$task->id}}">
        <div class="modal-dialog modal-lg">  
          <div class="modal-content bg-primary">
            <div class="modal-header">
              <h4 class="modal-title">{{__('tasks.User')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-12">
                    <div class="post">
                      <div class="user-block">
                        <span class="description text-white">Start Date - {{$task->start_date}}</span>
                        <span class="description text-white">Due Date - {{$task->end_date}}</span>
                        
                        <hr>
                        <span class="description text-white">Task Description - {{$task->description}}</span>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

      
              <!-----Edit Tasks  --->
        <div class="modal fade" id="edit-{{$task->id}}">
        <div class="modal-dialog modal-lg">  
          <div class="modal-content bg-primary">
            <div class="modal-header">
              <h4 class="modal-title">{{__('tasks.Status')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="{{url('edit-task', array($task->id))}}" method="post">
                @csrf
              <div class="row">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">

                  <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <select class="form-control custom-select" name="status">
                  <option value="{{$task->status}}">{{$task->status}}</option>
                  <option value="1">Completed</option>
                </select>
                </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Submit</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>




               <!-----Response Tasks  --->
        <div class="modal fade" id="response-{{$task->id}}">
        <div class="modal-dialog modal-lg">  
          <div class="modal-content bg-success">
            <div class="modal-header">
              <h4 class="modal-title">{{__('tasks.Response')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="{{url('response-task', array($task->id))}}" method="post">
                @csrf
              <div class="row">
              <div class="col-lg-8">
                 <div class="input-group mb-3">
                    <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('tasks.Feedback')}}" name="response">
                </div>
              </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Submit</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>


                   <!-----Reason For Timed Out Task --->

        <div class="modal fade" id="reason-{{$task->id}}">
        <div class="modal-dialog modal-lg">  
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">{{__('tasks.Reason')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="{{url('reason-task', array($task->id))}}" method="post">
                @csrf
              <div class="row">
              <div class="col-lg-8">
                 <div class="input-group mb-3">
                    <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('tasks.Reason_feedback')}}" name="reason">
                </div>
              </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Submit</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endif


      <!--Modal to sign In for attending the work -->


      <div class="modal fade" id="signIn-{{$user->employee_Number}}">
        <div class="modal-dialog modal-lg">  
          <div class="modal-content bg-success">
            <div class="modal-header">
              <h4 class="modal-title">{{__('user.User_attenadnce')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
          
              <div class="row">
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                   <div class="input-group date" id="reservationdate" data-target-input="nearest">
                        <input type="date" class="form-control " name="attendance_date" />
                        <div class="input-group-append"  data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>
                </div>
                 <div class="col-lg-6">
                <div class="input-group mb-3">
                  <select class="form-control custom-select" name="attendance_day">
                  <option selected disabled>{{__('user.User_day')}}</option>
                  <option value="Monday">Monday</option>
                  <option value="Tuesday">Tuesday</option>
                  <option value="Wednesday">Wednesday</option>
                  <option value="Thursday">Thursday</option>
                  <option value="Friday">Friday</option>
                </select>
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-sun"></i></span>
                  </div>
                </div>
                </div>
            </div>
                 <div class="row">
              <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-clock"></i></span>
                  </div>
                 <input type="time" class="form-control" placeholder="{{__('user.User_time_in')}}" name="time_in">
                </div>
                </div>
                <div class="col-lg-6">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-clock"></i></span>
                  </div>
                  <input type="time" class="form-control" placeholder="{{__('user.User_time_out')}}" name="time_out">
                </div>
              </div>
            </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Submit</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endif





<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
@include('Pages.User.Styles.js')
</body>
</html>
