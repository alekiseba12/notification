<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{__('user.User_Activation')}}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  @include('Pages.User.Styles.css')
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition lockscreen">
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
  <div class="lockscreen-logo">
    <a href="../../index2.html"><b>{{__('user.User_Activation_title')}}</b>{{__('user.User_Page')}}</a>
  </div>
  <!-- User name -->


  <!-- START LOCK SCREEN ITEM -->
  @foreach($users as $user)
  <div class="lockscreen-item">
    
    <!-- lockscreen image -->
    <div class="lockscreen-image">
      <img src="{{$user->pic}}" alt="User Image">
    </div>
    <!-- /.lockscreen-image -->

    <!-- lockscreen credentials (contains the form) -->

    <!-- /.lockscreen credentials -->
    
  </div>
   @endforeach
   <br><br>
  <!-- /.lockscreen-item -->
  @if(empty($user->salary))
          <div class="alert alert-danger">
            {{__('user.User_Alert')}}
            
          </div>
          @endif
  <div class="lockscreen-footer text-center">
    Copyright &copy; 2016-2020 <b><a href="javascript:;" class="text-black">Nycemedia</a></b><br>
    All rights reserved
  </div>
</div>
<!-- /.center -->

<!-- jQuery -->
@include('Pages.User.Styles.js')
</body>
</html>
