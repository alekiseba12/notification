<section class="content">
      <div class="container-fluid">
      
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-success">
          <div class="card-header">
            <h3 class="card-title">{{__('employees.Employee_form')}}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <form class="form-horizontal" method="post" action="{{url('update-employee-profile', array($employee->employee_Number))}}" enctype="multipart/form-data">
            @csrf
           <div class="card-body">
             <div class="row">
              <div class="col-lg-4">
                <div class="input-group">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                  </div>
                <select class="form-control custom-select" name="user_id">
                  <option value="{{$user->id}}">{{$user->name}}</option>
                  @foreach($users as $user)
                  <option value="{{$user->id}}">{{$user->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-lg-4">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="email" class="form-control" placeholder="{{__('employees.Employee_email')}}" name="email" value="{{$employee->email}}">
                </div>
              </div>
                 <div class="col-lg-4">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('employees.Employee_fullname')}}" name="fullnames" value="{{$employee->fullnames}}">
                </div>
              </div>
            </div>
              <h5 class="mt-4 mb-2">{{__('employees.Employee_gender')}}</h5>

                <div class="row">
               <div class="col-lg-6">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><input type="radio"></span>
                      </div>
                      <input type="text" class="form-control" placeholder="{{__('employees.Employee_male')}}" name="gender" value="female">
                    </div>
                    <!-- /input-group -->
                  </div>
                  <!-- /.col-lg-6 -->
                  <div class="col-lg-6">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><input type="radio"></span>
                      </div>
                      <input type="text" class="form-control" placeholder="{{__('employees.Employee_female')}}" name="gender" value="male" >
                    </div>
                    <!-- /input-group -->
                  </div>
                  <!-- /.col-lg-6 -->
                </div>
                <br>
             
             <div class="row">
                <div class="col-lg-6">
                <div class="input-group mb-3">
                  <input type="text" class="form-control" placeholder="{{__('employees.Employee_address')}}" name="address" value="{{$employee->address}}">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                  </div>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="{{__('employees.Employee_location')}}" name="location" value="{{$employee->location}}">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                  </div>
                </div>
              </div>
            </div>
                  <div class="row">
                <div class="col-lg-6">
                <div class="input-group mb-3">
                  <input type="text" class="form-control" placeholder="{{__('employees.Employee_phone')}}" name="phone" value="{{$employee->phone}}">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-phone"></i></span>
                  </div>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="{{__('employees.Employee_id')}}" name="national_id" value="{{$employee->national_id}}">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                </div>
              </div>
            </div>
                 <div class="row">
                <div class="col-lg-6">
                <div class="input-group mb-3">
                  <select class="form-control custom-select" name="education" value="{{$employee->education}}">
                  <option value="{{$employee->education}}">{{$employee->education}}</option>
                  <option value="Secondary School">Secondary School</option>
                  <option value="College">College</option>
                  <option value="University">University</option>
                </select>
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-graduation-cap"></i></span>
                  </div>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="input-group">

                  <input type="text" class="form-control" placeholder="{{__('employees.Employee_skill')}}" name="skills" value="{{$employee->skills}}">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6">
                <div class="input-group">
                <select class="form-control custom-select" name="type">
                  <option value="{{$employee->type}}">{{$employee->type}}</option>
                  <option value="Permannet">Permanent</option>
                  <option value="Contract">Contract</option>
                </select>
              </div>
            </div>
             <div class="col-lg-6">
                <div class="input-group">
                <input type="text" id="inputSpentBudget" class="form-control" name="salary" placeholder="{{__('employees.Employee_salary')}}" value="{{$employee->salary}}">
            </div>
          </div>

              </div>
              <br>
                 <div class="row">
                  <div class="col-lg-6">
                <div class="input-group">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                  </div>
                     <select class="form-control custom-select" name="role_no">
                         @foreach($roles as $role)
                        <option value="{{$role->role_no}}">{{$role->role}}</option>
                        @endforeach          
                </select>
                </div>
              </div>
                 <div class="col-lg-6">
                <div class="input-group">
                  <div class="input-group-append">
                      <span class="input-group-text"><i class="fas fa-home"></i></span>
                    </div>
                     <select class="form-control custom-select" name="department_No">
                         @foreach($departments as $depart)
                        <option value="{{$depart->department_No}}">{{$depart->department_Name}}</option>
                        @endforeach          
                </select>
                  
                  </div>
                </div>
              </div>
              <br>
              <div class="row">
               <div class="col-lg-6">
                <div class="input-group">
                <select class="form-control custom-select" name="status">
                  <option value="{{$employee->status}}">{{$employee->status}}</option>
                  <option value="0">Unverified</option>
                  <option value="1">Verified</option>
                  <option value="2">Declined</option>
                </select>
            </div>
          </div>
        </div>
            </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Submit</button>
                 
                </div>
            </form>

  
        </div>
     
      </div><!-- /.container-fluid -->
    </section>