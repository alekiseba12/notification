
<section class="content">
      <div class="container-fluid">
      
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-success">
          <div class="card-header">
            <h3 class="card-title">{{__('employees.Employee_form')}}</h3>
            &nbsp; &nbsp;
               <a class="btn btn-warning btn-sm" href="javascript:;" data-toggle="modal" data-target="#register">
                              <i class="fas fa-user">
                              </i>
                              Register
                          </a>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <form class="form-horizontal" method="post" action="{{route('create-employee')}}" enctype="multipart/form-data">
            @csrf
           <div class="card-body">
             <div class="row">
              <div class="col-lg-6">
                <div class="input-group">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                  </div>
                <select class="form-control custom-select" name="user_id">
                  <option selected disabled>Select {{__('employees.Employee_user')}}</option>
                  @foreach($users as $user)
                  <option value="{{$user->id}}">{{$user->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-lg-6">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('employees.Employee_fullname')}}" name="fullnames">
                </div>
              </div>
            </div>
              <h5 class="mt-4 mb-2">{{__('employees.Employee_gender')}}</h5>
             <div class="input-group mb-3">
                  <div class="input-group">
                  <select class="form-control custom-select" name="gender">
                  <option selected disabled>{{__('employees.Employee_gender')}}</option>
                  <option value="Male">Male</option>
                  <option value="Female">Female</option>               
                </select>
                    </div>
                    <!-- /input-group -->
                  </div>   
                <br>
             
             <div class="row">
                <div class="col-lg-6">
                <div class="input-group mb-3">
                  <input type="text" class="form-control" placeholder="{{__('employees.Employee_address')}}" name="address">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                  </div>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="{{__('employees.Employee_location')}}" name="location">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                  </div>
                </div>
              </div>
            </div>
                  <div class="row">
                <div class="col-lg-6">
                <div class="input-group mb-3">
                  <input type="text" class="form-control" placeholder="{{__('employees.Employee_phone')}}" name="phone">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-phone"></i></span>
                  </div>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="{{__('employees.Employee_id')}}" name="national_id">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                </div>
              </div>
            </div>
                 <div class="row">
                <div class="col-lg-6">
                <div class="input-group mb-3">
                  <select class="form-control custom-select" name="education">
                  <option selected disabled>Select {{__('employees.Employee_education')}}</option>
                  <option value="Secondary School">Secondary Schooll</option>
                  <option value="College">College</option>
                  <option value="University">University</option>
                </select>
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-graduation-cap"></i></span>
                  </div>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="input-group">

                  <input type="text" class="form-control" placeholder="{{__('employees.Employee_skill')}}" name="skills">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4">
                <div class="input-group">
                <select class="form-control custom-select" name="type">
                  <option selected disabled>Select {{__('employees.Employee_type')}}</option>
                  <option value="Permannet">Permanent</option>
                  <option value="Contract">Contract</option>
                </select>
              </div>
            </div>
             <div class="col-lg-4">
                <div class="input-group">
                <input type="text" id="inputSpentBudget" class="form-control" name="salary" placeholder="{{__('employees.Employee_salary')}}">
            </div>
          </div>
               <div class="col-lg-4">
                <div class="input-group">
                <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile" name="pic">
                        <label class="custom-file-label" for="exampleInputFile">{{__('employees.Employee_pic')}}</label>
                      </div>
              </div>
            </div>

              </div>
              <br>
                 <div class="row">
                  <div class="col-lg-6">
                <div class="input-group">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                  </div>
                     <select class="form-control custom-select" name="role_no">
                        <option>-----Choose {{__('employees.Employee_role')}}-------</option>
                         @foreach($roles as $role)
                        <option value="{{$role->role_no}}">{{$role->role}}</option>
                        @endforeach          
                </select>
                </div>
              </div>
                 <div class="col-lg-6">
                <div class="input-group">
                  <div class="input-group-append">
                      <span class="input-group-text"><i class="fas fa-home"></i></span>
                    </div>
                     <select class="form-control custom-select" name="department_No">
                        <option>-----Choose {{__('employees.Employee_department')}}-------</option>
                         @foreach($departments as $depart)
                        <option value="{{$depart->department_No}}">{{$depart->department_Name}}</option>
                        @endforeach          
                </select>
                  
                  </div>
                </div>

              </div><br>
               <h5 class="mt-4 mb-2">{{__('employees.Employee_description')}}</h5>
               <div class="mb-3">
                <textarea class="textarea" placeholder="Place some text here" name="description" 
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              </div>
            </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Submit</button>
                 
                </div>
            </form>

  
        </div>

        <!--Modal to register new user -->
        <div class="modal fade" id="register">
        <div class="modal-dialog modal-lg">
          <div class="modal-content bg-success">
          <form action="{{ route('register') }}" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">Register new user</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
              <div class="modal-body">
              <div class="row">
              <div class="col-lg-6">
             <div class="input-group mb-3">
            <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-user"></i></span>
            </div>
            <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="{{ __('Name') }}" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
            @error('name')
          <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
          </span>
          @enderror
            </div>
              </div>
              <div class="col-lg-6">
             <div class="input-group mb-3">
            <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-envelope"></i></span>
            </div>
           <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
          @error('email')
          <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
          </span>
          @enderror 
            </div>
              </div>
            </div>
            <div class="row">
            <div class="col-lg-6">
             <div class="input-group mb-3">
            <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-lock"></i></span>
            </div>
            <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="{{ __('Password') }}" name="password" required autocomplete="current-password">
          @error('password')
          <span class="invalid-feedback" role="alert">
         <strong>{{ $message }}</strong>
          </span>
          @enderror
            </div>
              </div>
               <div class="col-lg-6">
             <div class="input-group mb-3">
            <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-lock"></i></span>
            </div>
           <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="{{ __('Confirm Password') }}" name="password_confirmation" required autocomplete="new-password">
            </div>
              </div>
            </div>
            <br>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Submit</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
     
      </div><!-- /.container-fluid -->
    </section>