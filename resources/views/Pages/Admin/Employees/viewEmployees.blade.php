<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 @include('Pages.Admin.Templates.adminTitle')
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  @include('Pages.Admin.Styles.css')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
 @include('Pages.Admin.Templates.nav')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
 
   @include('Pages.Admin.Templates.sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('employees.Employee_team')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{{__('employees.Employee_title')}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
   
    <section class="content">

      <!-- Default box -->
      <div class="card card-success card-outline">
        <div class="card-header">
          <a class="btn btn-success btn-sm" href="{{route('employee-registration-form')}}">
                              <i class="fas fa-plus">
                              </i>
                              New Employee
                          </a>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 10%">
                          Ref No
                      </th>
                      <th style="width: 20%">
                          Fullnames
                      </th>
                      <th style="width: 30%">
                          Gender
                      </th>
                      <th>
                          Role
                      </th>
                      <th style="width: 8%" class="text-center">
                          Status
                      </th>
                      <th style="width: 20%">
                      </th>
                  </tr>
              </thead>
              @foreach($employees as $employee)
              <tbody>
                  <tr>
                      <td>
                           {{$employee->employee_Number}}
                      </td>
                      <td>
                          <a>
                              {{$employee->fullnames}}
                          </a>
                        
                      </td>
                      <td>
                          <ul class="list-inline">
                           @if($employee->gender=='male')
                              <li class="list-inline-item">
                                  <img alt="Avatar" class="table-avatar" src="img/avatar5.png">
                              </li>
                              @elseif($employee->gender=='female')
                              <li class="list-inline-item">
                                  <img alt="Avatar" class="table-avatar" src="img/avatar3.png">
                              </li>
                              @endif
                          </ul>
                      </td>
                      <td class="project_progress">
                          <div class="progress progress-sm">
                              <div class="progress-bar bg-green" role="progressbar" aria-volumenow="100" aria-volumemin="0" aria-volumemax="100" style="width: 100%">
                              </div>
                          </div>
                          <small>
                              {{$employee->role_no}}
                          </small>
                      </td>
                      <td class="project-state">
                        @if($employee->status==0)
                          <span class="badge badge-warning">Unverified</span>
                          @elseif($employee->status==1)

                           <span class="badge badge-success">Verified</span>

                            @elseif($employee->status==2)

                            <span class="badge badge-danger">Inactive</span>

                            @endif
                      </td>
                      <td class="project-actions text-right">
                          <a class="btn btn-primary btn-sm" href='{{url("employee-profile/{$employee->employee_Number}")}}'>
                              <i class="fas fa-folder">
                              </i>
                              View
                          </a>
                          <a class="btn btn-info btn-sm" href='{{url("edit-employee-profile/{$employee->employee_Number}")}}'>
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a href="javascript:;" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#employeeId-{{$employee->employee_Number}}">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                      </td>
                  </tr>
                
                 
              </tbody>
              @endforeach
          </table>
        </div>
   
        <!-- /.card-body -->
      </div>
      @foreach($employees as $employ)
      <div class="modal fade" id="employeeId-{{$employ->employee_Number}}">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
          <form action="{{url('delete-employee-profile', array($employ->employee_Number))}}" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">Employee | {{$employ->fullnames}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
               <input type=hidden id="employee_Number" name="employee_Number">
              <p>Are you sure want to delete this Employee &hellip; ?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Submit</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endforeach
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
     @include('Pages.Admin.Templates.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
@include('Pages.Admin.Styles.js')
<!-- ./wrapper -->

<!-- jQuery -->
<script>
    $('.addAttr').click(function() {
    var id = $(this).data('employee_Number');      
    $('#employee_Number').val(employee_Number);  
    } );
 </script>

</body>
</html>
