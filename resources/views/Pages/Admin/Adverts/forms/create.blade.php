<section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-warning">
          <div class="card-header">
            <h3 class="card-title">{{__('adverts.Advert_form')}}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <form class="form-horizontal" method="post" action="{{route('create-advert')}}">
            @csrf
           <div class="card-body">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('adverts.Advert_name')}}" name="advert_name">
                </div>
                 <div class="row">
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-clock"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('adverts.Advert_period')}}" name="advert_period_time">
                </div>
                </div>
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa--map-marker-alt"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('adverts.Advert_location')}}" name="location">
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <select class="form-control custom-select" name="client_No">
                        <option>-----Choose {{__('adverts.Advert_client')}}-------</option>
                         @foreach($clients as $client)
                        <option value="{{$client->client_No}}">{{$client->company}}</option>
                        @endforeach          
                </select>
                </div>
              </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Submit</button>
                 
                </div>
            </form>
        </div>
       <div class="row">
              <div class="col-md-10">
                <!-- DIRECT CHAT -->
                <div class="card card-warning card-outline direct-chat direct-chat-warning">
                  <div class="card-header">
                    <h3 class="card-title">{{__('adverts.list')}}</h3>

                    <div class="card-tools">
                      <span data-toggle="tooltip" title="3 New Messages" class="badge badge-warning">{{$allAdverts}}</span>
                      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body p-0">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>#No</th>
                      <th>Advert</th>
                      <th>Client</th>
                      <th>Period (Months)</th>
                      <th>Status</th>
                      <th>Start Date</th>
                    </tr>
                  </thead>
                  <tbody>
                   @foreach($adverts as $advert)
                    <tr>
                      <td>{{$advert->advert_No}}</td>
                      <td>{{$advert->advert_name}}</td>
                      <td>{{$advert->client_No}}</td>
                        <td class="project_progress">
                          <div class="progress progress-sm">
                            @if($advert->status==0)
                              <div class="progress-bar bg-danger" role="progressbar" aria-volumenow="100" aria-volumemin="0" aria-volumemax="100" style="width: {{$advert->advert_period_time}}%"></div>
                                @elseif($advert->status==1)
                                <div class="progress-bar bg-green" role="progressbar" aria-volumenow="100" aria-volumemin="0" aria-volumemax="100" style="width: {{$advert->advert_period_time}}% ">
                              </div>
                               @elseif($advert->status==2)
                                <div class="progress-bar bg-warning" role="progressbar" aria-volumenow="100" aria-volumemin="0" aria-volumemax="100" style="width: {{$advert->advert_period_time}}% ">
                              </div>
                               @elseif($advert->status==3)
                                <div class="progress-bar bg-info" role="progressbar" aria-volumenow="100" aria-volumemin="0" aria-volumemax="100" style="width: {{$advert->advert_period_time}}%">
                              </div>
                              @endif
                          </div>
                          <small>
                            {{$advert->advert_period_time}}  
                          </small>
                      </td>
                      <td class="project-state">
                        @if($advert->status==0)
                          <span class="badge badge-danger">Declined</span>
                          @elseif($advert->status==1)
                          <span class="badge badge-success">Active</span>
                          @elseif($advert->status==2)
                          <span class="badge badge-warning">Completed</span>
                          @elseif($advert->status==3)
                          <span class="badge badge-info">Halted</span>
                          @endif
                      </td>
                       <td>{{$advert->created_at}}</td>
                      <td class="project-actions text-right">
                            <a href="javascript:;" class="btn btn-info btn-sm" data-toggle="modal" data-target="#advertId-{{$advert->advert_No}}">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a href="javascript:;" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#keyAdvertId-{{$advert->advert_No}}">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
                  <!-- /.card-body -->

                  <!-- /.card-footer-->
                </div>
                <!--/.direct-chat -->
              </div>
              <!-- /.col -->

              <div class="col-md-2">
            <!-- Info Boxes Style 2 -->
            <div class="info-box mb-3 bg-warning">
              <span class="info-box-icon"><i class="fas fa-tag"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Completed</span>
                <span class="info-box-number">{{$completedStatus}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box mb-3 bg-success">
              <span class="info-box-icon"><i class="far fa-heart"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Active </span>
                <span class="info-box-number">{{$activeStatus}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box mb-3 bg-danger">
              <span class="info-box-icon"><i class="fas fa-trash"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Declined </span>
                <span class="info-box-number">{{$declinedStatus}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box mb-3 bg-info">
              <span class="info-box-icon"><i class="far fa-comment"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Halted</span>
                <span class="info-box-number">{{$inactiveStatus}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
       
              <!-- /.col -->
            </div>

     
      </div><!-- /.container-fluid -->



      @foreach($adverts as $key)
      <div class="modal fade" id="advertId-{{$key->advert_No}}">
        <div class="modal-dialog">
          <div class="modal-content bg-info">
          <form action="{{url('update-advert', array($key->advert_No))}}" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{__('adverts.Edit')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
               <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('adverts.Advert_name')}}" name="advert_name" value="{{$key->advert_name}}">
                </div>
                 <div class="row">
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-clock"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('adverts.Advert_period')}}" name="advert_period_time" value="{{$key->advert_period_time}}">
                </div>
                </div>
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa--map-marker-alt"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('adverts.Advert_location')}}" name="location" value="{{$key->location}}">
                </div>
              </div>
            </div>
            <div class="row">
            <div class="col-lg-6">
            <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <select class="form-control custom-select" name="client_No">
                        <option>-----Choose {{__('adverts.Advert_client')}}-------</option>
                         @foreach($clients as $client)
                        <option value="{{$client->client_No}}">{{$client->company}}</option>
                        @endforeach          
                </select>
                </div>
              </div>
                <div class="col-lg-6">
            <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <select class="form-control custom-select" name="status">
                  <option value="{{$key->status}}">{{$key->status}}</option>
                  <option value="0">Decline</option>
                  <option value="1">Activate</option>
                  <option value="2">Completed</option>
                   <option value="3">Halted</option>
                </select>
                </div>
              </div>
            </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Update</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endforeach
  

       @foreach($adverts as $keyAdvert)
      <div class="modal fade" id="keyAdvertId-{{$keyAdvert->advert_No}}">
        <div class="modal-dialog">  
          <div class="modal-content bg-danger">
          <form action="{{url('delete-advert', array($keyAdvert->advert_No))}}" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{__('adverts.Delete')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <input type=hidden id="mat_no" name="mat_no">
              <p>Are you sure want to delete this Advert &hellip; ?  {{$keyAdvert->advert_name}}</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Destroy</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endforeach
    </section>