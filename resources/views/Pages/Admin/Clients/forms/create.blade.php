<section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">{{__('clients.Client_form')}}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <form class="form-horizontal" method="post" action="{{route('create-client')}}" enctype="multipart/form-data">
            @csrf
           <div class="card-body">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-home"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('clients.Client_company')}}" name="company">
                </div>
                 <div class="row">
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('clients.Client_email')}}" name="email">
                </div>
                </div>
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-phone"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('clients.Client_phone')}}" name="phone">
                </div>
              </div>
            </div>

             
             <div class="row">
                <div class="col-lg-6">
                <div class="input-group mb-3">
                  <input type="text" class="form-control" placeholder="{{__('clients.Client_address')}}" name="address">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                  </div>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="{{__('clients.Client_location')}}" name="location">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                  </div>
                </div>
              </div>
            </div>
             <div class="row">
             <div class="input-group col-lg-6">
                 <div class="input-group">
                  <input type="text" class="form-control" placeholder="{{__('clients.Client_percentage')}}" name="percentage">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                </div>
                </div>

                <div class="col-lg-6">
                <div class="input-group">
                <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile" name="pic">
                        <label class="custom-file-label" for="exampleInputFile">{{__('clients.Client_logo')}}</label>
                      </div>
              </div>
              </div>
            </div>
              </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Submit</button>
                 
                </div>
            </form>

  
        </div>
     
      </div><!-- /.container-fluid -->
    </section>