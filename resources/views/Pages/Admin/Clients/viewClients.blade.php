<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 @include('Pages.Admin.Templates.adminTitle')
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  @include('Pages.Admin.Styles.css')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
 @include('Pages.Admin.Templates.nav')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
 
   @include('Pages.Admin.Templates.sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
               <a class="btn btn-app">
                  <span class="badge bg-danger">{{$countClients}}</span>
                   <h1>{{__('clients.Client_title')}}</h1>
                </a>

          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{{__('clients.Client_title')}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
   
    <section class="content">
            <div class="card card-warning card-outline">
                <div class="card-header">
                <a class="btn btn-warning btn-sm" href="{{route('client-registration-form')}}">
                              <i class="fas fa-plus">
                              </i>
                              New Client
                          </a>
              </div>
              <div class="card-body p-0">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th style="width: 10px"># Client No</th>
                      <th>Company</th>
                      <th>Email</th>
                      <th style="width: 40px">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($clients as $client)
                    <tr>
                      <td>{{$client->client_No}}</td>
                      <td>{{$client->company}}</td>
                      <td>
                      {{$client->email}}
                      </td>
                       <td>
                        @if($client->status==0)
                        <span class="badge bg-danger">Declined</span>
                        @elseif($client->status==1)
                        <span class="badge bg-success">Active</span>

                        @elseif($client->status==2)
                        <span class="badge bg-warning">Deactivated</span>
                    
                        @endif
                      </td>
                    
                         <td class="project-actions text-right">
                          <a href="javascript:;" class="btn btn-success btn-sm" data-toggle="modal" data-target="#viewclientId-{{$client->client_No}}">
                              <i class="fas fa-folder">
                              </i>
                              View
                          </a>
                         <a href="javascript:;" class="btn btn-info btn-sm" data-toggle="modal" data-target="#keyClientId-{{$client->client_No}}">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a href="javascript:;" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#clieRoleId-{{$client->client_No}}">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                      </td>
                    </tr>
                    <tr>
                      @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          <div class="row">
          <div class="col-md-6">
            <div class="card card-success card-outline">
              <div class="card-header">
                <h3 class="card-title">{{__('clients.Active_Clients')}}</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Company</th>
                      <th>Contract Range</th>
                      <th style="width: 40px">Label</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($actives as $advert)
                    <tr>
                      <td>{{$loop->iteration}}</td>
                      <td>{{$advert->company}}</td>
                      <td>
                        
                        <div class="progress progress-sm">
                        @if($advert->percentage >=70)
                        <div class="progress-bar bg-success" style="width: {{$advert->percentage}}%"></div>
                        @elseif($advert->percentage >=50 && $advert->percentage <=70)
                        <div class="progress-bar bg-warning" style="width: {{$advert->percentage}}%"></div>
                        @elseif($advert->percentage >=30 && $advert->percentage <=50)
                        <div class="progress-bar bg-primary" style="width: {{$advert->percentage}}%"></div>
                        @elseif($advert->percentage >=02 && $advert->percentage <=30)
                        <div class="progress-bar bg-danger" style="width: {{$advert->percentage}}%"></div>
                        @endif
                      </div>
                      </td>
                      <td>
                        @if($advert->percentage >=70)
                        <span class="badge bg-success">{{$advert->percentage}}%</span>
                        @elseif($advert->percentage >=50 && $advert->percentage <=70)
                          <span class="badge bg-warning">{{$advert->percentage}}%</span>
                        @elseif($advert->percentage >=30 && $advert->percentage <=50)
                          <span class="badge bg-primary">{{$advert->percentage}}%</span>
                        @elseif($advert->percentage >=02 && $advert->percentage <=30)
                        <span class="badge bg-danger">{{$advert->percentage}}%</span>
                        @endif
                        
                      </td>
                    </tr>
                    @endforeach
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                </ul>
              </div>
            </div>
            <!-- /.card -->

            <!-- /.card -->
          </div>
          <!-- /.col -->
       <div class="col-md-6">
            <div class="card card-danger card-outline">
              <div class="card-header">
                <h3 class="card-title">{{__('clients.Inactive_Clients')}}</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Company</th>
                      <th>Contract Range</th>
                      <th style="width: 40px">Label</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($inactives as $advert)
                    <tr>
                      <td>{{$loop->iteration}}</td>
                      <td>{{$advert->company}}</td>
                      <td>
                        
                        <div class="progress progress-sm">
                        @if($advert->percentage >=70)
                        <div class="progress-bar bg-success" style="width: {{$advert->percentage}}%"></div>
                        @elseif($advert->percentage >=50 && $advert->percentage <=70)
                        <div class="progress-bar bg-warning" style="width: {{$advert->percentage}}%"></div>
                        @elseif($advert->percentage >=30 && $advert->percentage <=50)
                        <div class="progress-bar bg-primary" style="width: {{$advert->percentage}}%"></div>
                        @elseif($advert->percentage >=02 && $advert->percentage <=30)
                        <div class="progress-bar bg-danger" style="width: {{$advert->percentage}}%"></div>
                        @endif
                      </div>
                      </td>
                      <td>
                        @if($advert->percentage >=70)
                        <span class="badge bg-success">{{$advert->percentage}}%</span>
                        @elseif($advert->percentage >=50 && $advert->percentage <=70)
                          <span class="badge bg-warning">{{$advert->percentage}}%</span>
                        @elseif($advert->percentage >=30 && $advert->percentage <=50)
                          <span class="badge bg-primary">{{$advert->percentage}}%</span>
                        @elseif($advert->percentage >=02 && $advert->percentage <=30)
                        <span class="badge bg-danger">{{$advert->percentage}}%</span>
                        @endif
                        
                      </td>
                    </tr>
                    @endforeach
                    
                  </tbody>
                </table>
              </div>
            </div>
            <!-- /.card -->

            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>

      <!--View Modal for Clients -->
           @foreach($clients as $key)
      <div class="modal fade" id="viewclientId-{{$key->client_No}}">
        <div class="modal-dialog  modal-lg">
          <div class="modal-content bg-success">
            <div class="modal-header">
              <h4 class="modal-title">{{__('clients.View_Client')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                   <div class="row">
            <div class="col-12 col-md-12 col-lg-12 order-2 order-md-1">
              <div class="row">
                <div class="col-12 col-sm-4">
                  <div class="info-box bg-light">
                    <div class="info-box-content">
                      <span class="info-box-text text-center text-muted">Number of adverts</span>
                      <span class="info-box-number text-center text-muted mb-0">23</span>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-4">
                  <div class="info-box ">
                    <div class="info-box-content">
                      <span class="info-box-text text-center text-muted">Total amount paid</span>
                      <span class="info-box-number text-center text-muted mb-0">USD 3.6M</span>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-4">
                  <div class="info-box bg-light">
                    <div class="info-box-content">
                      <span class="info-box-text text-center text-muted">Total adverts duration</span>
                      <span class="info-box-number text-center text-muted mb-0">20 <span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                    <div class="post">
                      <div class="user-block">
                        <img class="img-circle img-bordered-sm" src="{{$key->pic}}" alt="user image">
                        <span class="username">
                          <a href="#" class="text text-white">{{$key->company}}.</a>
                        </span>
                        <span class="description text-white">Created Date - {{\Carbon\Carbon::parse($key->created_at)->format('d/m/Y')}}</span>
                        <span class="description text-white">Email - {{$key->email}}</span>
                        <span class="description text-white">Phone- {{$key->phone}}</span>
                        <span class="description text-white">Address - {{$key->address}}</span>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endforeach
  

       @foreach($clients as $keyClient)
      <div class="modal fade" id="keyClientId-{{$keyClient->client_No}}">
        <div class="modal-dialog modal-lg">
          <div class="modal-content bg-info">
          <form action="{{url('update-client', array($keyClient->client_No))}}" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{__('clients.Edit_Client')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-home"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('clients.Client_company')}}" name="company" value="{{$keyClient->company}}">
                </div>
                 <div class="row">
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('clients.Client_email')}}" name="email" value="{{$keyClient->email}}">
                </div>
                </div>
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-phone"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('clients.Client_phone')}}" name="phone" value="{{$keyClient->phone}}">
                </div>
              </div>
            </div>

             
             <div class="row">
                <div class="col-lg-6">
                <div class="input-group mb-3">
                  <input type="text" class="form-control" placeholder="{{__('clients.Client_address')}}" name="address" value="{{$keyClient->address}}">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                  </div>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="{{__('clients.Client_location')}}" name="location" value="{{$keyClient->location}}">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                  </div>
                </div>
              </div>
            </div>
             <div class="row">
                <div class="col-lg-6">
                <div class="input-group mb-3">
                  <input type="text" class="form-control" placeholder="{{__('clients.Client_percentage')}}" name="percentage" value="{{$keyClient->percentage}}">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="input-group">
                  <select class="form-control custom-select" name="status">
                  <option value="{{$keyClient->status}}">{{$keyClient->status}}</option>
                  <option value="0">Decline</option>
                  <option value="1">Activate</option>
                  <option value="2">Deactive</option>
                 
                </select>
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                </div>
              </div>
            </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Edit</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endforeach


      @foreach($clients as $clie)
      <div class="modal fade" id="clieRoleId-{{$clie->client_No}}">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
          <form action="{{url('delete-client', array($clie->client_No))}}" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{__('clients.Delete_Client')}} | {{$clie->company}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <input type=hidden id="client_No" name="client_No">
              <p>Are you sure want to delete this Client &hellip; ?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Destroy</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endforeach


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
     @include('Pages.Admin.Templates.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
@include('Pages.Admin.Styles.js')
<!-- ./wrapper -->

<!-- jQuery -->

</body>
</html>
