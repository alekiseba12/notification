<section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-danger card-outline">
          <div class="card-header">
            <h3 class="card-title">{{__('department.Department_form')}}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <form class="form-horizontal" method="post" action="{{route('create-department')}}" enctype="multipart/form-data">
            @csrf
           <div class="card-body">
                <div class="row">
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-home"></i></span>
                  </div>
                 <input type="text" class="form-control" placeholder="{{__('department.Department_name')}}" name="department_Name">
                </div>
                </div>
                <div class="col-lg-6">
                <div class="input-group">
                <div class="custom-file">
                <input type="file" class="custom-file-input" id="exampleInputFile" name="pic">
                <label class="custom-file-label" for="exampleInputFile">{{__('department.Department_pic')}}</label>
                </div>
              </div>
              </div>
            </div>
                 <div class="row">
              <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-users"></i></span>
                  </div>
                 <input type="text" class="form-control" placeholder="{{__('department.Department_employees')}}" name="employees">
                </div>
                </div>
                <div class="col-lg-6">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('department.Department_description')}}" name="description">
                </div>
              </div>
            </div>
              </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Submit</button>
                  
                </div>
            </form>

  
        </div>
     
      </div>
            <div class="row">
              <div class="col-md-8">
                <!-- DIRECT CHAT -->
               <div class="card card-warning">
              <div class="card-header border-transparent">
                <h3 class="card-title">{{__('department.Departments')}}</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                      <th>Ref No</th>
                      <th>Department</th>
                      <th>Status</th>
                      
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($departments as $depart)
                    <tr>
                      <td>{{$depart->department_No}}</td>
                      <td>{{$depart->department_Name}}</td>
                      <td> 
                        @if($depart->status==0)
                        <span class="badge badge-danger">Inactive</span>
                        @elseif($depart->status==1)
                         <span class="badge badge-success">Active</span>

                         @elseif($depart->status==2)
                         <span class="badge badge-warning">Fair Working</span>
                         @elseif($depart->status==3)
                         <span class="badge badge-primary">Poor Delivery</span>

                         @endif
                      </td>
                      <td class="project-actions text-right">
                        <a href="javascript:;" class="btn btn-info btn-sm" data-toggle="modal" data-target="#departmentId-{{$depart->department_No}}">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a href="javascript:;" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#keyDepartmentId-{{$depart->department_No}}">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                      </td>
                    </tr>
                   
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
              <!-- /.card-footer -->
            </div>
                <!--/.direct-chat -->
              </div>
              <!-- /.col -->

              <div class="col-md-4">
                <!-- USERS LIST -->
              <div class="card-body p-0">
                @foreach($departments as $departy)
                <ul class="products-list product-list-in-card pl-2 pr-2">
                  <li class="item">
                    <div class="product-img">
                      <img src="{{$departy->pic}}" alt="Product Image" class="img-size-50">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">{{$departy->department_Name}}
                        <span class="badge badge-warning float-right">{{$departy->employees}}</span></a>
                      <span class="product-description">
                       {{$departy->description}}.
                      </span>
                    </div>
                  </li>
                  <!-- /.item -->
                </ul>
                @endforeach
              </div>
              <!-- /.card-body -->
              <!-- /.card-footer -->
            </div>
             <div class="col-md-12">
              
                    <p class="text-center">
                      <strong>{{__('department.Performance')}}</strong>
                    </p>
                      @foreach($departments as $progressDep)
                    <div class="progress-group">
                      {{$progressDep->department_Name}}
                      <span class="float-right"><b>{{$progressDep->percentage}}</b>/100</span>
                      <div class="progress progress-sm">
                        @if($progressDep->percentage >=70)
                        <div class="progress-bar bg-success" style="width: {{$progressDep->percentage}}%"></div>
                        @elseif($progressDep->percentage >=50 && $progressDep->percentage <=70)
                        <div class="progress-bar bg-warning" style="width: {{$progressDep->percentage}}%"></div>
                        @elseif($progressDep->percentage >=30 && $progressDep->percentage <=50)
                        <div class="progress-bar bg-primary" style="width: {{$progressDep->percentage}}%"></div>
                        @elseif($progressDep->percentage >=02 && $progressDep->percentage <=30)
                        <div class="progress-bar bg-danger" style="width: {{$progressDep->percentage}}%"></div>
                        @endif
                      </div>
                    </div>
                     @endforeach
                    <!-- /.progress-group -->

             
                   
                    <!-- /.progress-group -->
                  </div>

                <!--/.card -->
              </div>
              <!-- /.col -->
            </div>
      @foreach($departments as $key)
      <div class="modal fade" id="departmentId-{{$key->department_No}}">
        <div class="modal-dialog modal-lg">
          <div class="modal-content bg-info">
          <form action="{{url('update-department', array($key->department_No))}}" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{__('department.Edit')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-lg-4">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-home"></i></span>
                  </div>
                 <input type="text" class="form-control" placeholder="{{__('department.Department_name')}}" name="department_Name" value="{{$key->department_Name}}">
                </div>
                </div>
                <div class="col-lg-4">
                <div class="input-group">
                <div class="product-img">
                <img src="{{$key->pic}}" alt="Product Image" class="img-size-50">
                </div>
               </div>
             </div>
             <div class="col-lg-4">
                <div class="input-group">
                 <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-users"></i></span>
                  </div>
                 <input type="text" class="form-control" placeholder="{{__('department.Department_employees')}}" name="employees" value="{{$key->employees}}">
                
              </div>
              </div>
            </div>
            <br>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('department.Department_description')}}" name="description" value="{{$key->description}}">
                </div>

                 <div class="row">
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <select class="form-control custom-select" name="status">
                  <option value="{{$key->status}}">{{$key->status}}</option>
                  <option value="0">Inactive</option>
                  <option value="1">Active</option>
                  <option value="2">Fair</option>
                   <option value="3">Poor</option>
                </select>
                </div>
                </div>
                <div class="col-lg-6">
               <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                 <input type="text" class="form-control" placeholder="{{__('department.Department_reason')}}" name="reason" value="{{$key->reason}}">
                </div>
              </div>
            </div>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <select class="form-control custom-select" name="user_id">
                  <option value="{{$key->user}}">{{$key->status}}</option>
                  <option value="0">Inactive</option>
                  <option value="1">Active</option>
                  <option value="2">Fair</option>
                   <option value="3">Poor</option>
                </select>
                  <input type="text" class="form-control" placeholder="{{__('department.Department_percentage')}}" name="percentage" value="{{$key->percentage}}">
                </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Update</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endforeach

       @foreach($departments as $keyDepartment)
      <div class="modal fade" id="keyDepartmentId-{{$keyDepartment->department_No}}">
        <div class="modal-dialog">  
          <div class="modal-content bg-danger">
          <form action="{{url('delete-department', array($keyDepartment->department_No))}}" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{__('department.Delete')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <input type=hidden id="department_No" name="department_No">
              <p>Are you sure want to delete this Department &hellip; ?  {{$keyDepartment->department_Name}}</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Destroy</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endforeach
    </section>
    <br><br>