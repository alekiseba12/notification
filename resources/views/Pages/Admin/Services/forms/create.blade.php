<section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-warning card-outline">
          <div class="card-header">
            <h3 class="card-title">{{__('services.Service_form')}}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <form class="form-horizontal" method="post" action="{{route('create-service')}}">
            @csrf
           <div class="card-body">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-gear"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('services.Service_name')}}" name="service">
                </div>
                 <div class="row">
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('services.Service_description')}}" name="description">
                </div>
                </div>
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                  </div>
                       <select class="form-control custom-select" name="employee_Number">
                        <option>-----Choose {{__('services.Service_username')}}-------</option>
                         @foreach($employees as $employee)
                        <option value="{{$employee->employee_Number}}">{{$employee->fullnames}}</option>
                        @endforeach          
                </select>
                 
                </div>
              </div>
            </div>

              </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Submit</button>
                  
                </div>
            </form>

  
        </div>
     
      </div>
       <div class="row">
          <div class="col-md-6">
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">{{__('services.Service')}}</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-striped">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">S/N</th>
                      <th>Service</th>
                      <th>Description</th>
                      <th style="width: 40px">User </th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($services as $service)
                    <tr>
                      <td>{{$service->service_No}}</td>
                      <td>{{$service->service}}</td>
                      <td>
                       {{$service->description}}
                      </td>
                      <td><span class="badge bg-warning">{{$service->fullnames}}</span></td>
                         <td class="project-actions text-right">
                          <a href="javascript:;" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#serviceId-{{$service->service_No}}">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a href="javascript:;" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#keyServiceId-{{$service->service_No}}">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                      </td>
                    </tr>
                    @endforeach
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                </ul>
              </div>
            </div>
            <!-- /.card -->

            <!-- /.card -->
          </div>
          <!-- /.col -->
           <div class="col-md-6">
            <div class="card card-info card-outline">
              <div class="card-header">
                <h3 class="card-title">{{__('services.Service_Slider')}}</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                  </ol>
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img class="d-block w-100" src="https://placehold.it/900x500/39CCCC/ffffff&text=I+Love+Bootstrap" alt="First slide">
                    </div>
                    <div class="carousel-item">
                      <img class="d-block w-100" src="https://placehold.it/900x500/3c8dbc/ffffff&text=I+Love+Bootstrap" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                      <img class="d-block w-100" src="https://placehold.it/900x500/f39c12/ffffff&text=I+Love+Bootstrap" alt="Third slide">
                    </div>
                  </div>
                  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div><!-- /.container-fluid -->

        @foreach($services as $serviceKey)
      <div class="modal fade" id="serviceId-{{$serviceKey->service_No}}">
        <div class="modal-dialog">
          <div class="modal-content bg-info">
          <form action="{{url('delete-client', array($serviceKey->service_No))}}" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{__('service.Service_Edit')}} | </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-gear"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('services.Service_name')}}" name="service" value="{{$serviceKey->service}}">
                </div>
                 <div class="row">
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('services.Service_description')}}" name="description" value="{{$serviceKey->description}}">
                </div>
                </div>
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                  </div>
                       <select class="form-control custom-select" name="employee_Number">
                        <option>-----Choose {{__('services.Service_username')}}-------</option>
                         @foreach($employees as $employee)
                        <option value="{{$employee->employee_Number}}">{{$employee->fullnames}}</option>
                        @endforeach          
                </select>
                 
                </div>
              </div>
            </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Destroy</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endforeach


       @foreach($services as $keyService)
      <div class="modal fade" id="keyServiceId-{{$keyService->service_No}}">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
          <form action="{{url('delete-service', array($keyService->service_No))}}" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{__('services.Service_Delete')}} | {{$keyService->service}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <input type=hidden id="role_no" name="role_no">
              <p>Are you sure want to delete this Role &hellip; ?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Delete</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endforeach
    </section>
