<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 @include('Pages.Admin.Templates.adminTitle')
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  @include('Pages.Admin.Styles.css')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
 @include('Pages.Admin.Templates.nav')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
 
   @include('Pages.Admin.Templates.sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('services.View_service')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{{__('services.Service_title')}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
   
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <a class="btn btn-success btn-sm" href="{{route('employee-registration-form')}}">
                              <i class="fas fa-plus">
                              </i>
                              New Service
                          </a>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 10%">
                          Service No
                      </th>
                      <th style="width: 20%">
                          Servive
                      </th>
                      <th style="width: 30%">
                          Descritption
                      </th>
                      <th>
                          User Incharge
                      </th>
                     
                  </tr>
              </thead>
              @foreach($employees as $employee)
              <tbody>
                  <tr>
                      <td>
                           {{$employee->employee_Number}}
                      </td>
                      <td>
                          <a>
                              {{$employee->firstname}}
                          </a>
                          <br/>
                          <small>
                              {{$employee->lastname}}
                          </small>
                      </td>
                      <td>
                          <ul class="list-inline">
                           @if($employee->gender=='male')
                              <li class="list-inline-item">
                                  <img alt="Avatar" class="table-avatar" src="img/avatar5.png">
                              </li>
                              @elseif($employee->gender=='female')
                              <li class="list-inline-item">
                                  <img alt="Avatar" class="table-avatar" src="img/avatar3.png">
                              </li>
                              @endif
                          </ul>
                      </td>
                      <td class="project_progress">
                          <div class="progress progress-sm">
                              <div class="progress-bar bg-green" role="progressbar" aria-volumenow="100" aria-volumemin="0" aria-volumemax="100" style="width: 100%">
                              </div>
                          </div>
                          <small>
                              Chief Executive Officer
                          </small>
                      </td>
                      <td class="project-state">
                          <span class="badge badge-success">Active</span>
                      </td>
                      <td class="project-actions text-right">
                          <a class="btn btn-primary btn-sm" href="#">
                              <i class="fas fa-folder">
                              </i>
                              View
                          </a>
                          <a class="btn btn-info btn-sm" href="#">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a class="btn btn-danger btn-sm" href="#">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                      </td>
                  </tr>
                
                 
              </tbody>
              @endforeach
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
     @include('Pages.Admin.Templates.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
@include('Pages.Admin.Styles.js')
<!-- ./wrapper -->

<!-- jQuery -->

</body>
</html>
