 <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="/img/logo2.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Nycemedia</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->

      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
         @if(isset($user->id ) )
        <div class="image">
          <img src="/{{$user->pic}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{$user->fullnames}}</a>
        </div>
         @endif
      </div>


      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="{{route('admin')}}" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                {{__('templates.Company_dashboard')}}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
          </li>
            <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                {{__('templates.Company_employees')}}
                  <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
               <li class="nav-item">
                <a href="{{route('employee-registration-form')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('templates.Add_Employee')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('management-team')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('templates.View_Employee')}}</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="UI/icons.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('templates.Employee_Attandance')}}</p>
                </a>
              </li>
              
            </ul>
          </li>
             <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
              <p>
                {{__('templates.Roles')}}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
               <li class="nav-item">
                <a href="{{route('new-role')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('templates.Add_Role')}}</p>
                </a>
              </li>
 
              
            </ul>
          </li>

           <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                {{__('templates.Company_clients')}}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('view-clients')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('templates.View_Clients')}}</p>
                </a>
              </li>
              
            </ul>
          </li>

           <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                {{__('templates.Company_services')}}
                 <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
               <li class="nav-item">
                <a href="{{route('new-service')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('templates.Add_Service')}}</p>

                </a>
              </li>
              
            </ul>
          </li>

           <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>
                {{__('templates.Company_departments')}}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
               <li class="nav-item">
                <a href="{{route('new-department')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('templates.Add_Department')}}</p>
                </a>
              </li>

              
            </ul>
          </li>

           <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-plus"></i>
              <p>
                {{__('templates.Company_gadgets')}}
                <i class="right fas fa-angle-left"></i>

              </p>
            </a>
            <ul class="nav nav-treeview">
               <li class="nav-item">
                <a href="{{route('new-gadget')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('templates.Add_Gadget')}}</p>
                </a>
              </li>
              
            </ul>
          </li>
           <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-car"></i>
              <p>
                {{__('templates.Company_matatus')}}
              <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
               <li class="nav-item">
                <a href="{{route('new-matatu')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('templates.Add_Matatu')}}</p>
                </a>
              </li>
              
            </ul>
          </li>

            <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-envelope"></i>
              <p>
                {{__('templates.Company_Adverts')}}
              <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
               <li class="nav-item">
                <a href="{{route('new-advert')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('templates.Add_Advert')}}</p>
                </a>
              </li>
              
            </ul>

          </li>
             <li class="nav-item">
            <a href="{{route('new-contract')}}" class="nav-link">
              <i class="nav-icon fas fa-calendar"></i>
              <p>
               {{__('templates.Contacts')}}
                <span class="right badge badge-warning">Hot</span>
              </p>
            </a>
          </li>
             <li class="nav-item">
            <a href="{{route('new-task')}}" class="nav-link">
              <i class="nav-icon fas fa-list"></i>
              <p>
               {{__('templates.Company_task')}}
                <span class="right badge badge-success">View</span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="widgets.html" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
               {{__('templates.Company_portfolio')}}
                <span class="right badge badge-danger">New</span>
              </p>
            </a>
          </li>


        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>