  <footer class="main-footer">
    <strong>Copyright &copy; 2016-2020 <a href="javascript:;">Nycemedia Managment System</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 8.4.0
    </div> 
  </footer>