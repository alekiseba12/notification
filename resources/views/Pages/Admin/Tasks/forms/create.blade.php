<section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-warning">
          <div class="card-header">
            <h3 class="card-title">{{__('tasks.Task_form')}}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <form class="form-horizontal" method="post" action="{{route('create-advert')}}">
            @csrf
           <div class="card-body">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('tasks.Task_name')}}" name="task_name">
                </div>
                 <div class="row">
                <div class="col-lg-4">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-clock"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('tasks.Task_description')}}" name="description">
                </div>
                </div>
                <div class="col-lg-4">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">
                  </div>
                  <input type="date" class="form-control" placeholder="{{__('tasks.Task_start')}}" name="start_date">
                </div>
              </div>
                   <div class="col-lg-4">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">
                  </div>
                  <input type="date" class="form-control" placeholder="{{__('tasks.Task_end')}}" name="end_date">
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                  </div>
                  <select class="form-control custom-select" name="user_id">
                        <option>-----Choose {{__('tasks.Task_user')}}-------</option>
                         @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach          
                </select>
                </div>
              </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Submit</button>
                 
                </div>
            </form>
        </div>
       <div class="row">
              <div class="col-md-12">
                <!-- DIRECT CHAT -->
                <div class="card card-warning card-outline direct-chat direct-chat-warning">
                  <div class="card-header">
                    <h3 class="card-title">{{__('tasks.List')}}</h3>

                    <div class="card-tools">
                      <span data-toggle="tooltip" title="3 New Messages" class="badge badge-warning">{{$allCompanyTasks}}</span>
                      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body p-0">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>#No</th>
                      <th>Task</th>
                      <th>Description</th>
                      <th>Employee</th>
                      <th>Start Date</th>
                      <th>End Date</th>
                      <th>Status</th>
                      <th>Response</th>
                      <th>Reason</th>
                      <th></th>
                      
                    </tr>
                  </thead>
                  <tbody>
                   @foreach($tasks as $task)
                    <tr>
                      <td>{{$loop->iteration}}</td>
                      <td>{{$task->task_name}}</td>
                      <td>{{$task->description}}</td>
                      <td>{{$task->name}}</td>
                      <td>{{$task->start_date}}</td>
                      <td>{{$task->end_date}}</td>
                      <td class="project-state">
                        @if($task->status==0)
                          <span class="badge badge-warning">Assigned</span>
                          @elseif($task->status==1)
                          <span class="badge badge-success">Completed</span>
                          @elseif($task->status==2)
                          <span class="badge badge-danger">Timed Out</span>
                         
                          @endif
                      </td>
                      <td>
                       @if($task->status==1)
                        {{$task->response}}
                        @elseif(empty($task->response =null))
                        
                        No Data
                        @endif
                      </td>
                      <td> 
                       @if($task->status==2)
                        {{$task->reason}}
                        @elseif(empty($task->reason))

                        No Data
                        @endif
                      </td>
                      <td class="project-actions text-right">
                            <a href="javascript:;" class="btn btn-info btn-sm" data-toggle="modal" data-target="#taskId-{{$task->id}}">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a href="javascript:;" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#keyTaskId-{{$task->id}}">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                      </td>
                      
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
                  <!-- /.card-body -->

                  <!-- /.card-footer-->
                </div>
                <!--/.direct-chat -->
              </div>
              <!-- /.col -->  
            </div><!-- /.container-fluid -->



      @foreach($tasks as $key)
      <div class="modal fade" id="taskId-{{$key->id}}">
        <div class="modal-dialog modal-lg">
          <div class="modal-content bg-info">
          <form action="{{url('update-task', array($key->id))}}" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{__('task.Edit')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
               <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('tasks.Task_name')}}" name="task_name" value="{{$key->task_name}}">
                </div>
                 <div class="row">
                <div class="col-lg-4">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-clock"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('tasks.Task_description')}}" name="description" value="{{$key->description}}">
                </div>
                </div>
                <div class="col-lg-4">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">
                  </div>
                  <input type="date" class="form-control" placeholder="{{__('tasks.Task_start')}}" name="start_date" value="{{$key->start_date}}">
                </div>
              </div>

                <div class="col-lg-4">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">
                  </div>
                  <input type="date" class="form-control" placeholder="{{__('tasks.Task_end')}}" name="end_date" value="{{$key->end_date}}">
                </div>
              </div>
            </div>
            <div class="row">
            <div class="col-lg-6">
            <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                  </div>
                  <select class="form-control custom-select" name="user_id">
                        <option>-----Choose {{__('tasks.Task_user')}}-------</option>
                         @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach          
                </select>
                </div>
              </div>
                <div class="col-lg-6">
            <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <select class="form-control custom-select" name="status">
                  <option value="{{$key->status}}">{{$key->status}}</option>
                  <option value="0">Assign</option>
                  <option value="1">Completed</option>
                  <option value="2">Decline</option>
                </select>
                </div>
              </div>
            </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Update</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endforeach
  

       @foreach($tasks as $keyTask)
      <div class="modal fade" id="keyTaskId-{{$keyTask->id}}">
        <div class="modal-dialog">  
          <div class="modal-content bg-danger">
          <form action="{{url('delete-task', array($keyTask->id))}}" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{__('task.Delete')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <input type=hidden id="mat_no" name="mat_no">
              <p>Are you sure want to delete this Task &hellip; ?  {{$keyTask->task_name}}</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Destroy</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endforeach
    </section>