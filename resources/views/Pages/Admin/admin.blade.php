<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  @include('Pages.Admin.Templates.adminTitle')
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  @include('Pages.Admin.Styles.css')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
 @include('Pages.Admin.Templates.nav')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
 
   @include('Pages.Admin.Templates.sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{__('templates.Company_title')}}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{{__('templates.Company_title')}}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
        <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{$employees}}</h3>

                <p>{{__('templates.Company_employees')}}</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="{{route('employee-registration-form')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{$clients}}<sup style="font-size: 20px"></sup></h3>

                <p>{{__('templates.Company_clients')}}</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="{{route('client-registration-form')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{$roles}}</h3>

                <p>{{__('templates.Roles')}}</p>
              </div>
              <div class="icon">
                <i class="ion ion-envelope"></i>
              </div>
              <a href="{{route('new-role')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>{{$departments}}</h3>

                <p>{{__('templates.Company_departments')}}</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="{{route('new-department')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
         <div class="row">
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">{{__('templates.Company_gadgets')}}</span>
                <span class="info-box-number">{{$gadgets}}</span>
              </div>
           
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-success"><i class="far fa-flag"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('templates.Matatus_Matatus')}}</span>
                <span class="info-box-number">{{$matatus}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-warning"><i class="far fa-envelope"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('templates.Company_Adverts')}}</span>
                <span class="info-box-number">{{$adverts}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-danger"><i class="fas fa-wifi"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">{{__('templates.Company_services')}}</span>
                <span class="info-box-number">{{$services}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row for likes-->
        <!-- Main row -->
              <div class="row">
              <div class="col-md-6">
                <!-- USERS LIST -->
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">{{__('templates.Company_management')}}</h3>

                    <div class="card-tools">
                      <span class="badge badge-danger">{{$employees}}</span>
                      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                      </button>

                    </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body p-0">
                    <ul class="users-list clearfix">
                      @foreach($managements as $manage)
                      <li>
                        <img src="{{$manage->pic}}" alt="User Image">
                        <a class="users-list-name" href="#">{{$manage->fullname}}</a>
                        <span class="users-list-date">{{$manage->gender}}</span>
                      </li>
                      @endforeach
                     
                    </ul>
                    <!-- /.users-list -->
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer text-center">
                    <a href="{{route('management-team')}}"class="btn btn-sm btn-warning float-left">{{__('templates.Company_all_employees')}}</a>
                  </div>
                  <!-- /.card-footer -->
                </div>
                <!--/.card -->
              </div>
              <div class="col-md-6">
             <div class="card">
              <div class="card-header">
                <h3 class="card-title">{{__('templates.Company_Clients')}}</h3>

                <div class="card-tools">
                  <span class="badge badge-success">{{$clients}} </span>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                @foreach($allClients as $client)
                <ul class="products-list product-list-in-card pl-2 pr-2">
                  <li class="item">
                    <div class="product-img">
                      <img src="{{$client->pic}}" alt="Product Image" class="img-size-50">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">{{$client->company}}

                        @if($client->status==0)
                        <span class="badge bg-danger float-right">Declined</span>
                        @elseif($client->status==1)
                        <span class="badge bg-success float-right">Active</span>

                        @elseif($client->status==2)
                        <span class="badge bg-warning float-right">Deactivated</span>
                    
                        @endif
                      <span class="product-description">
                        {{$client->email}}
                      </span>
                    </div>
                  </li>
                  @endforeach
                  <!-- /.item -->
                  
                </ul>
              </div>
              <!-- /.card-body -->
              <div class="card-footer text-center">
                <a href="{{route('view-clients')}}" class=" uppercase btn btn-sm btn-danger float-right">{{__('templates.View_Clients')}}</a>
              </div>
              <!-- /.card-footer -->
            </div>
          </div>

              <!-- /.card-body -->
         
              <!-- /.footer -->
            </div>

              <div class="row">
              <div class="col-md-8">
                <!-- USERS LIST -->
                <div class="card">
              <div class="card-header border-transparent">
                <h3 class="card-title">{{__('templates.Company_all_services')}}</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                      <th>Order ID</th>
                      <th>Service</th>
                      <th>Manager</th>
                      <th>Description</th>
                     
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($companyServices as $services)
                    <tr>
                      <td><a href="javascript::">{{$services->service_No}}</a></td>
                       <td>
                        <span class="badge badge-warning">{{$services->service}}</span></td>
                      <td>{{$services->fullnames}}</td>
                      <td>
                        <div class="sparkbar" data-color="#00a65a" data-height="20">{{$services->description}}</div>
                      </td>
                    </tr>
                    @endforeach
                    
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <a href="{{route('new-service')}}" class="btn btn-sm btn-success float-right">View All Orders</a>
              </div>
              <!-- /.card-footer -->
            </div>
                <!--/.card -->
              </div>
              <div class="col-md-4">
             <div class="card">
              <div class="card-header">
                <h3 class="card-title">{{__('templates.Company_played_advert')}}</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
              <div class="info-box mb-3 bg-warning">
              <span class="info-box-icon"><i class="fas fa-tag"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Completed</span>
                <span class="info-box-number">{{$completedStatus}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box mb-3 bg-success">
              <span class="info-box-icon"><i class="far fa-heart"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Active</span>
                <span class="info-box-number">{{$activeStatus}}<span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box mb-3 bg-danger">
              <span class="info-box-icon"><i class="fas fa-cloud-download-alt"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Declined</span>
                <span class="info-box-number">{{$inactiveStatus}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box mb-3 bg-info">
              <span class="info-box-icon"><i class="far fa-comment"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Halted</span>
                <span class="info-box-number">{{$declinedStatus}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
              </div>
              <!-- /.card-body -->
      
              <!-- /.card-footer -->
            </div>
              <!-- /.card-body -->
         
              <!-- /.footer -->
            </div>
              <!-- /.col -->
            </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
     @include('Pages.Admin.Templates.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
@include('Pages.Admin.Styles.js')
<!-- ./wrapper -->

<!-- jQuery -->

</body>
</html>
