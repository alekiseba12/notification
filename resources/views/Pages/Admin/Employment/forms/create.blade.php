<section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-danger card-outline">
          <div class="card-header">
            <h3 class="card-title">{{__('employments.Employment_form')}}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <form class="form-horizontal" method="post" action="{{route('create-contract')}}" enctype="multipart/form-data">
            @csrf
           <div class="card-body">
               <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                 <input type="text" class="form-control" placeholder="{{__('employments.Emplyment_name')}}" name="contract" >
                </div>
                <div class="row">
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-users"></i></span>
                  </div>
                  <select class="form-control custom-select" name="user_id">
                  <option selected disabled>{{__('employments.Emplyment_user')}}</option>
                  @foreach($users as $user)
                  <option value="{{$user->id}}">{{$user->name}}</option>
                  @endforeach
                </select>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="input-group">
                <div class="custom-file">
                <input type="file" class="custom-file-input" id="exampleInputFile" name="document">
                <label class="custom-file-label" for="exampleInputFile">{{__('employments.Employment_document')}}</label>
                </div>
              </div>
              </div>
      
            </div>
              </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Submit</button>
                  
                </div>
            </form>

  
        </div>
     
      </div>
            <div class="row">
              <div class="col-md-12">
                <!-- DIRECT CHAT -->
               <div class="card card-danger card-outline">
              <div class="card-header border-transparent">
                <h3 class="card-title">{{__('employments.list')}}</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                      <th>Ref No</th>
                      <th>Employee</th>
                      <th>Contract Name</th>
                      <th>Document</th>
                      <th>Status</th>
                      
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($employments as $employment)
                    <tr>
                      <td>{{$employment->employment_No}}</td>
                      <td>{{$employment->name}}</td>
                      <td>{{$employment->contract}}</td>
                      <td>
                         @if($employment->status==0)
                       <a href="{{asset($employment->document)}}" download=""><span class="badge bg-success"><i class="fa fa-download"> View</i></span></a>
                       @elseif($employment->status==1)
                       <a href="{{asset($employment->document)}}" download=""><span class="badge bg-danger"><i class="fa fa-download"> Check</i></span></a>
                       @elseif($employment->status==2)
                       <a href="{{asset($employment->document)}}" download=""><span class="badge bg-warning"><i class="fa fa-download"> Check</i></span></a>
                         @elseif($employment->status==3)
                       <a href="{{asset($employment->document)}}" download=""><span class="badge bg-info"><i class="fa fa-download"> Check</i></span></a>
                         @endif
                        </td>
                      <td> 
                        @if($employment->status==0)
                        <span class="badge badge-success">New</span>
                        @elseif($employment->status==1)
                         <span class="badge badge-danger">Completed</span>
                         @elseif($employment->status==2)
                         <span class="badge badge-warning">Terminated</span>
                          @elseif($employment->status==3)
                         <span class="badge badge-info">Renewed</span>
                         @endif
                      </td>
                      <td class="project-actions text-right">
                        <a href="javascript:;" class="btn btn-info btn-sm" data-toggle="modal" data-target="#departmentId-{{$employment->employment_No}}">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a href="javascript:;" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#keyDepartmentId-{{$employment->employment_No}}">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                      </td>
                    </tr>
                   
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
              <!-- /.card-footer -->
            </div>
                <!--/.direct-chat -->
              </div>
              <!-- /.col -->

                <!--/.card -->
              </div>
              <!-- /.col -->
            </div>
      @foreach($employments as $key)
      <div class="modal fade" id="departmentId-{{$key->employment_No}}">
        <div class="modal-dialog modal-lg">
          <div class="modal-content bg-info">
          <form action="{{url('update-contract', array($key->employment_No))}}" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{__('employments.Edit')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
              <div class="modal-body">
              <div class="row">
              <div class="col-lg-4">
             <div class="input-group mb-3">
            <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-envelope"></i></span>
            </div>
            <input type="text" class="form-control" placeholder="{{__('employments.Emplyment_name')}}" name="contract" value="{{$key->contract}}">
            </div>
              </div>
                <div class="col-lg-4">
                <div class="input-group">
                  <select class="form-control custom-select" name="status">
                  <option value="{{$key->status}}">{{$key->status}}</option>
                  <option value="0">New</option>
                  <option value="1">Completed</option>
                  <option value="2">Terminated</option>
                   <option value="3">Renewed</option>
                </select>
               </div>
             </div>
             <div class="col-lg-4">
                <div class="input-group">
                 <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-users"></i></span>
                  </div>
                  <select class="form-control custom-select" name="user_id">
                   @foreach($users as $user)
                  <option value="{{$user->id}}">{{$user->name}}</option>
                  @endforeach
                  
                </select>
                
              </div>
              </div>
            </div>
            <br>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Update</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endforeach

       @foreach($employments as $keyEmployment)
      <div class="modal fade" id="keyDepartmentId-{{$keyEmployment->employment_No}}">
        <div class="modal-dialog">  
          <div class="modal-content bg-danger">
          <form action="{{url('delete-contract', array($keyEmployment->employment_No))}}" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{__('employments.Delete')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <input type=hidden id="department_No" name="department_No">
              <p>Are you sure want to delete this Contract  &hellip; ?  </p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Destroy</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endforeach
    </section>
    <br><br>