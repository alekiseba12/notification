<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 @include('Pages.Admin.Templates.adminTitle')
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  @include('Pages.Admin.Styles.css')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
 @include('Pages.Admin.Templates.nav')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
 
   @include('Pages.Admin.Templates.sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('stock.Bars')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{{__('stock.Stock_title')}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
   
    <section class="content">

      <!-- Default box -->
      <div class="card card-danger card-outline">
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 10%">
                          Bar No
                      </th>
                      <th style="width: 20%">
                          Bar
                      </th>
                    
                      <th style="width: 20%">
                      </th>
                  </tr>
              </thead>
              @foreach($bars as $bar)
              <tbody>
                  <tr>
                      <td>
                           {{$bar->bar_no}}
                      </td>
                      <td>
                          <a>
                              {{$bar->bar_name}}
                          </a>
                         
                      </td>
                      <td class="project-actions text-right">
                          <a class="btn btn-primary btn-sm" href='{{url("all-bar-products/{$bar->bar_no}")}}'>
                              <i class="fas fa-folder">
                              </i>
                              Products
                          </a>
                          <a class="btn btn-info btn-sm" href="#">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a class="btn btn-danger btn-sm" href="#">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                      </td>
                  </tr>
                
                 
              </tbody>
              @endforeach
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
     @include('Pages.Admin.Templates.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
@include('Pages.Admin.Styles.js')
<!-- ./wrapper -->

<!-- jQuery -->

</body>
</html>
