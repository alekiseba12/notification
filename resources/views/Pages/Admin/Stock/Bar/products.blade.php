<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 @include('Pages.Admin.Templates.adminTitle')
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  @include('Pages.Admin.Styles.css')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
 @include('Pages.Admin.Templates.nav')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
 
   @include('Pages.Admin.Templates.sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('stock.View_Bar_Products')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{{__('stock.Stock_title')}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
   
    <section class="content">

      <!-- Default box -->
      <div class="card card-danger card-outline">
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 40%">
                          Product No
                      </th>
                      <th style="width: 20%">
                         Product
                      </th>
                    
                      <th style="width: 20%">
                        Stock
                      </th>
                  </tr>
              </thead>
              @foreach($barProducts as $product)
              <tbody>
                  <tr>
                      <td>
                           {{$product->product_no}}
                      </td>
                      <td>
                          <a>
                              {{$product->product}}
                          </a>
                         
                      </td>
                        <td>
                          <a>
                              {{$product->stock}}
                          </a>
                         
                      </td>
                  </tr>
                
                 
              </tbody>
              @endforeach
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
     @include('Pages.Admin.Templates.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
@include('Pages.Admin.Styles.js')
<!-- ./wrapper -->

<!-- jQuery -->

</body>
</html>
