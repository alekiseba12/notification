<section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="row">
        <div class="col-md-6">
        <div class="card card-danger card-outline">
          <div class="card-header">
            <h3 class="card-title">{{__('stock.View_products')}}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
                <table class="table table-striped">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">Code</th>
                      <th>Product</th>
                      <th>Stock</th>

                    </tr>
                  </thead>
                  <tbody>
                       @foreach($limitProducts as $product)
                    <tr>
                      <td>{{$product->product_no}}</td>
                      <td>{{$product->product}}</td>
                      <td>{{$product->stock}}</td>
                   
                  @endforeach
                  </tbody>
                </table>
            <div class="card-footer clearfix">
                  <a class="btn btn-warning btn-sm" href="#">
                              <i class="fas fa-eye">
                              </i>
                              View More
                          </a>
              </div>
        </div>

      </div>

         <div class="col-md-6">
            <div class="card card-secondary card-outline">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#timeline" data-toggle="tab">{{__('stock.County_form')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">{{__('stock.Region_form')}}</a></li>
                  <li class="nav-item"><a class="nav-link" href="#bar" data-toggle="tab">{{__('stock.Bar_form')}}</a></li>
                  <li class="nav-item"><a class="nav-link " href="#activity" data-toggle="tab">{{__('stock.Product_form')}}</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                
                  <!-- /.tab-pane -->
                  <div class="active tab-pane" id="timeline">
                   <form class="form-horizontal" method="post" action="{{route('create-county')}}">
                           @csrf
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Select {{__('stock.County')}}</label>
                        <div class="col-sm-10">
                         <select class="form-control custom-select" name="county">
                        <option>------------</option>
                        <option value="Nairobi">Nairobi</option>
                        <option value="Mombasa">Mombasa</option>
                        <option value="Kakamega">Kakamega</option>
                        <option value=" Kisumu">Kisumu</option>
                        <option value="Nakuru">Nakuru</option>
                </select>
                        </div>
                      </div>   
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->

                  <div class="tab-pane" id="settings">
                   <form class="form-horizontal" method="post" action="{{route('create-region')}}">
                           @csrf
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('stock.Region')}}</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputName" name="region_name">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">{{__('stock.County')}}</label>
                        <div class="col-sm-10">
                        <select class="form-control custom-select" name="county_no">
                        <option>------------</option>
                        @foreach($counties as $county)
                        <option value="{{$county->county_no}}">{{$county->county}}</option>
                        @endforeach
                       
                </select>
              </div>
                      </div>
                      
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-warning">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>

                   <div class="tab-pane" id="bar">
                    <form class="form-horizontal" method="post" action="{{route('create-new-bar')}}">
                           @csrf
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">{{__('stock.Bar')}}</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputName" name="bar_name">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">{{__('stock.Region')}}</label>
                        <div class="col-sm-10">
                        <select class="form-control custom-select" name="region_no">
                        <option>-----Choose-------</option>
                         @foreach($regions as $region)
                        <option value="{{$region->region_no}}">{{$region->region_name}}</option>
                        @endforeach
                        
                </select>
              </div>
                      </div>
                      
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>

                    <div class="tab-pane" id="activity">
                       <form class="form-horizontal" method="post" action="{{route('create-new-product')}}">
                           @csrf
                 <div class="card-body">
                  <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                   <select class="form-control custom-select" name="product">
                  <option selected disabled>{{__('stock.Eabl')}}</option>
                  <option value="Guinness FES">Guinness FES</option>
                  <option value="Guinness Smooth">Guinness Smooth</option>
                  <option value="HH13">HH13</option>
                  <option value="Tusker Lager">Tusker Lager</option>
                  <option value="Tusker Cidar">Tusker Cidar</option>
                  <option value="usker Malt">Tusker Malt</option>
                  <option value="Tusker Lite">Tusker Lite</option>
                  <option value="Pilsner Lager">Pilsner Lager</option>
                  <option value="Balozi">Balozi</option>
                  <option value="Guarana">Guarana</option>
                  <option value="Sikera">Sikera</option>
                  <option value="Smirnoff">Smirnoff</option>
                  <option value="Gilbeys">Gilbeys</option>
                  <option value="Gordon's">Gordon's</option>
                  <option value="Chrome">Chrome</option>
                  <option value="Kenya Cane">Kenya Cane</option>
                  <option value="Johnnie Walker">Johnnie Walker</option>
                </select>
                </div>
                    <div class="row">
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-list"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('stock.Eabl_Stock')}}" name="stock">
                </div>
                </div>
                 <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-list"></i></span>
                  </div>
                 <select class="form-control custom-select" name="bar_no">
                        <option>-----Choose {{__('stock.Bar')}}-------</option>
                         @foreach($bars as $bar)
                        <option value="{{$bar->bar_no}}">{{$bar->bar_name}}</option>
                        @endforeach          
                </select>
                </div>
                </div>
            </div>
              </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-secondary">Submit</button>
                  
                </div>
                </form>
                    <!-- /.post -->
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          </div>
     
      </div>
       <div class="row">
          <div class="col-md-6">
            <div class="card card-warning card-outline">
              <div class="card-header">
            <h3 class="card-title">{{__('stock.Counties')}}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
              <div class="card-body">
                <table class="table table-striped">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">S/N</th>
                      <th>County</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($limitCounties as $county)
                    <tr>
                      <td>{{$county->county_no}}</td>
                      <td>{{$county->county}}</td>
              
              
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            <div class="card-footer clearfix">
                  <a class="btn btn-danger btn-sm" href="{{route('all-counties')}}">
                              <i class="fas fa-eye">
                              </i>
                              View More
                          </a>
              </div>
            </div>
            <!-- /.card -->

            <!-- /.card -->
          </div>
          <!-- /.col -->
            <div class="col-md-6">
            <div class="card card-success card-outline">
                   <div class="card-header">
            <h3 class="card-title">{{__('stock.Regions')}}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
              <div class="card-body">
                <table class="table table-striped">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">S/N</th>
                      <th>Region</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($limitRegions as $region)
                    <tr>
                      <td>{{$region->region_no}}</td>
                      <td>{{$region->region_name}}</td>
                        
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                  <a class="btn btn-info btn-sm" href="{{route('all-regions')}}">
                              <i class="fas fa-eye">
                              </i>
                              View More
                          </a>
              </div>
            </div>
            <!-- /.card -->

            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div><!-- /.container-fluid -->

         <div class="row">
          <div class="col-md-6">
            <div class="card card-warning card-outline">
                   <div class="card-header">
            <h3 class="card-title">{{__('stock.Bars')}}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
              <div class="card-body">
                <table class="table table-striped">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">S/N</th>
                      <th>Bar</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($limitBars as $bar)
                    <tr>
                      <td>{{$bar->bar_no}}</td>
                      <td>{{$bar->bar_name}}</td>

           
              
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                  <a class="btn btn-success btn-sm" href="{{route('all-bars')}}">
                              <i class="fas fa-eye">
                              </i>
                              View More
                          </a>
              </div>
            </div>
            <!-- /.card -->

            <!-- /.card -->
          </div>
          <!-- /.col -->
           <div class="col-md-6">
            <div class="card card-info card-outline">
              <div class="card-header">
                <h3 class="card-title">{{__('stock.Regions_Bars')}}</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                  </ol>
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img class="d-block w-100" src="https://placehold.it/900x500/39CCCC/ffffff&text=I+Love+Bootstrap" alt="First slide">
                    </div>
                    <div class="carousel-item">
                      <img class="d-block w-100" src="https://placehold.it/900x500/3c8dbc/ffffff&text=I+Love+Bootstrap" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                      <img class="d-block w-100" src="https://placehold.it/900x500/f39c12/ffffff&text=I+Love+Bootstrap" alt="Third slide">
                    </div>
                  </div>
                  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div><
    </section>
