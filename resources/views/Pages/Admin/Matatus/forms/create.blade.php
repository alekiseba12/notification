<section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-danger card-outline">
          <div class="card-header">
            <h3 class="card-title">{{__('matatus.Matatu_form')}}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <form class="form-horizontal" method="post" action="{{route('create-matatu')}}">
            @csrf
           <div class="card-body">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-car"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('matatus.Matatu_Name')}}" name="name">
                </div>
                 <div class="row">
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('matatus.Matatu_No')}}" name="no_plate">
                </div>
                </div>
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('matatus.Matatu_Routes')}}" name="mat_routes">
                </div>
              </div>
            </div>
              <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-users"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('matatus.Matatu_Passangers')}}" name="passengers">
                </div>

              </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Submit</button>
                  
                </div>
            </form>

  
        </div>
         <div class="card card-success card-outline">
              <div class="card-header border-transparent">
                <h3 class="card-title">{{__('gadgets.list')}}</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                      <th>Code</th>
                      <th>Name</th>
                      <th>No Plate</th>
                      <th>No Passangers</th>
                      <th>Routes</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($matatus as $matatu)
                    <tr>
                      <td>{{$matatu->mat_no}}</td>
                      <td>{{$matatu->name}}</td>
                      <td>
                        <span class="badge badge-danger">{{$matatu->no_plate}}</span>
                      </td>
                      <td>
                        <span class="badge badge-warning">{{$matatu->passengers}}</span>
                      </td>
                      <td>
                        <div class="sparkbar" data-color="#00a65a" data-height="20">{{$matatu->mat_routes}}</div>
                      </td>
                         <td class="project-actions text-right">
                          <a href="javascript:;" class="btn btn-info btn-sm" data-toggle="modal" data-target="#matatutId-{{$matatu->mat_no}}">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a href="javascript:;" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#keyMatatuId-{{$matatu->mat_no}}">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                      </td>
                    </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->

              <!-- /.card-footer -->
            </div>
     
      </div><!-- /.container-fluid -->


      @foreach($matatus as $key)
      <div class="modal fade" id="matatutId-{{$key->mat_no}}">
        <div class="modal-dialog">
          <div class="modal-content bg-info">
          <form action="{{url('update-matatu', array($key->mat_no))}}" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{__('matatus.Matatu_Edit')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-car"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('matatus.Matatu_Name')}}" name="name" value="{{$key->name}}">
                </div>
                 <div class="row">
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('matatus.Matatu_No')}}" name="no_plate" value="{{$key->no_plate}}">
                </div>
                </div>
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('matatus.Matatu_Routes')}}" name="mat_routes" value="{{$key->mat_routes}}">
                </div>
              </div>
            </div>
              <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-users"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('matatus.Matatu_Passangers')}}" name="passengers" value="{{$key->passengers}}">
                </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Update</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endforeach
  

       @foreach($matatus as $keyMat)
      <div class="modal fade" id="keyMatatuId-{{$keyMat->mat_no}}">
        <div class="modal-dialog">  
          <div class="modal-content bg-danger">
          <form action="{{url('delete-matatu', array($keyMat->mat_no))}}" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{__('gadget.Delete')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <input type=hidden id="mat_no" name="mat_no">
              <p>Are you sure want to delete this Mat &hellip; ?  {{$keyMat->name}}</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Destroy</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endforeach
    </section>