<section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-info card-outline">
          <div class="card-header">
            <h3 class="card-title">{{__('roles.Role_form')}}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <form class="form-horizontal" method="post" action="{{route('create-role')}}">
            @csrf
           <div class="card-body">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('roles.Role_name')}}" name="role">
                </div>

              </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Submit</button>
                  
                </div>
            </form>

  
        </div>
       <div class="row">
              <div class="col-md-12">
                <!-- DIRECT CHAT -->
               <div class="card card-warning card-outline">
              <div class="card-header border-transparent">
                <h3 class="card-title">{{__('roles.Roles')}}</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                      <th>Ref No</th>
                      <th>Role</th>
                      <th>Status</th>
                      
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($roles as $role)
                    <tr>
                      <td>{{$role->role_no}}</td>
                      <td>{{$role->role}}</td>
                      <td>
                        @if($role->status==0)
                        <span class="badge badge-warning">Unsigned</span>
                        @elseif($role->status==1)
                        <span class="badge badge-success">Assigned</span>
                        @elseif($role->status==2)
                        <span class="badge badge-danger">Removed</span>
                        @endif
                      </td>
                      <td class="project-actions text-right">
                          <a href="javascript:;" class="btn btn-info btn-sm" data-toggle="modal" data-target="#roleId-{{$role->role_no}}">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a href="javascript:;" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#keyRoleId-{{$role->role_no}}">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                      </td>
                    </tr>
                   @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
              <!-- /.card-footer -->
            </div>
                <!--/.direct-chat -->
              </div>

                <!--/.card -->
              </div>
      </div>
      <!--Edit Modal for Role -->
           @foreach($roles as $key)
      <div class="modal fade" id="roleId-{{$key->role_no}}">
        <div class="modal-dialog">
          <div class="modal-content bg-info">
          <form action="{{url('update-role', array($key->role_no))}}" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{__('roles.Edit')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                  <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('roles.Role_name')}}" name="role" value="{{$key->role}}">
                </div>

                    <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <select class="form-control custom-select" name="status">
                  <option value="{{$key->status}}">{{$key->status}}</option>
                  <option value="0">Unsigned</option>
                  <option value="1">Assigned</option>
                  <option value="2">Removed</option>
                </select>
                </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Edit</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endforeach
  

       @foreach($roles as $keyRole)
      <div class="modal fade" id="keyRoleId-{{$keyRole->role_no}}">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
          <form action="{{url('delete-role', array($keyRole->role_no))}}" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{__('roles.Delete')}} | {{$keyRole->role}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <input type=hidden id="role_no" name="role_no">
              <p>Are you sure want to delete this Role &hellip; ?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Delete</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endforeach

    </section>