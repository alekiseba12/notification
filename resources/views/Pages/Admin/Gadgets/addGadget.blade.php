<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 @include('Pages.Admin.Templates.adminTitle')
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  @include('Pages.Admin.Styles.css')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
 @include('Pages.Admin.Templates.nav')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
 
   @include('Pages.Admin.Templates.sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{__('gadgets.Gadget_title')}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">{{__('gadgets.Gadget_title')}}</li>
            </ol>
          </div>
          <br>
          @include('Pages.Admin.Services.forms.serviceSuccessMessage')
          @include('Pages.Admin.Services.forms.serviceErrorMessage')
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    @include('Pages.Admin.Gadgets.forms.create')
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
     @include('Pages.Admin.Templates.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
@include('Pages.Admin.Styles.js')
<!-- ./wrapper -->

<!-- jQuery -->

</body>
</html>
