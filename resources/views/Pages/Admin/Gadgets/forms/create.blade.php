<section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-success card-outline">
          <div class="card-header">
            <h3 class="card-title">{{__('gadgets.Gadget_form')}}</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <form class="form-horizontal" method="post" action="{{route('create-gadget')}}" enctype="multipart/form-data">
            @csrf
           <div class="card-body">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-gear"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('gadgets.Gadget_name')}}" name="name">
                </div>
                 <div class="row">
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('gadgets.Gadget_code')}}" name="code">
                </div>
                </div>
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('gadgets.Gadget_description')}}" name="description">
                </div>
              </div>
            </div>
                <div class="input-group mb-3">
                <div class="input-group">
                <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile" name="photo">
                        <label class="custom-file-label" for="exampleInputFile">{{__('gadgets.Gadget_photo')}}</label>
                      </div>
              </div>
            </div>

              </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Submit</button>
                  
                </div>
            </form>

  
        </div>
         <div class="card card-success">
              <div class="card-header border-transparent">
                <h3 class="card-title">{{__('gadgets.list')}}</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                      <th>Ref No</th>
                      <th>Name</th>
                      <th>Code</th>
                      <th>Gadget</th>
                      <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($gadgets as $gadget)
                    <tr>
                      <td>{{$gadget->gadget_code}}</td>
                      <td>{{$gadget->name}}</td>
                      <td>
                        @if($gadget->name=="Wi-Fi Router")
                        <span class="badge badge-danger">{{$gadget->code}}</span>
                        @elseif($gadget->name=="TV Box")
                         <span class="badge badge-warning">{{$gadget->code}}</span>
                         @endif
                      </td>
                      <td><div class="product-img">
                      <img src="{{$gadget->photo}}" alt="Product Image" class="img-size-50">
                    </div>
                  </td>
                      
                      <td>
                        <div class="sparkbar" data-color="#00a65a" data-height="20">{{$gadget->description}}</div>
                      </td>
                         <td class="project-actions text-right">
                          <a href="javascript:;" class="btn btn-info btn-sm" data-toggle="modal" data-target="#gadgetId-{{$gadget->gadget_code}}">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a href="javascript:;" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#keyGadgetId-{{$gadget->gadget_code}}">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                      </td>
                    </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->

              <!-- /.card-footer -->
            </div>
     
      </div><!-- /.container-fluid -->


      @foreach($gadgets as $key)
      <div class="modal fade" id="gadgetId-{{$key->gadget_code}}">
        <div class="modal-dialog">
          <div class="modal-content bg-info">
          <form action="{{url('update-gadget', array($key->gadget_code))}}" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{__('gadget.Edit')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                  <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-gear"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('gadgets.Gadget_name')}}" name="name" value="{{$key->name}}">
                </div>
                 <div class="row">
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('gadgets.Gadget_code')}}" name="code" value="{{$key->code}}">
                </div>
                </div>
                <div class="col-lg-6">
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">

                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="{{__('gadgets.Gadget_description')}}" name="description" value="{{$key->description}}">
                </div>
              </div>
            </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Update</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endforeach
  

       @foreach($gadgets as $keyGadget)
      <div class="modal fade" id="keyGadgetId-{{$keyGadget->gadget_code}}">
        <div class="modal-dialog">  
          <div class="modal-content bg-danger">
          <form action="{{url('delete-gadget', array($keyGadget->gadget_code))}}" method="post">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">{{__('gadget.Delete')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <input type=hidden id="gadget_code" name="gadget_code">
              <p>Are you sure want to delete this Gadget &hellip; ?  {{$keyGadget->name}}</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-outline-light">Destroy</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      @endforeach
    </section>