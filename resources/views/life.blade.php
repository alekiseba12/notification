@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
               <div class="row">
                    @if ($errors->any())
                  <div class="col-md-12 alert alert-danger">
                         <ul>
                           @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                           @endforeach
                               </ul>
                                </div>
                            @endif
                        </div>
            <div class="card">
                <div class="card-header text-center">{{ __('Booking Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                <div class="card-body">
 <form method="post" action="{{route('create_booking')}}">
    @csrf
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">{{ __('Company') }}</label>
      <input type="text" class="form-control" id="inputEmail4" name="company">
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">{{ __('Company Email') }}</label>
      <input type="email" class="form-control" id="inputPassword4" name="email">
    </div>
  </div>
  <div class="form-group">
    <label for="inputAddress">{{ __('Address') }}</label>
    <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St" name="address">
  </div>
  <div class="form-group">
    <label for="inputAddress2">{{ __('Phone Number') }}</label>
    <input type="text" class="form-control" id="inputAddress2" placeholder="+254 7XX-XXX-XXX" name="phone">
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputCity">{{ __('Company Location') }}</label>
      <input type="text" class="form-control" id="inputCity" name="location">
    </div>
    <div class="form-group col-md-4">
      <label for="inputState">{{ __('Select Service') }}</label>
      <select id="inputState" class="form-control" name="service">
        <option selected>Choose...</option>
        <option value="Cleaning">Cleaning</option>
        <option value="Photography">Photography</option>
        <option value="Food Delivery">Food Delivery</option>
        <option value="Renting">Renting</option>

      </select>
    </div>
    <div class="form-group col-md-2">
      <label for="inputZip">Zipcode/Postcode</label>
      <input type="text" class="form-control" id="inputZip" name="postcode">
    </div>
  </div>

  <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>

                  </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
