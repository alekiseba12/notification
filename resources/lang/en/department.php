<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    //Employee registration form
    'Department_title' => 'Departments',
    'Department_form' => 'Department Form',
    'Department_name' => 'Department',
    'Department_pic' => 'Department Photo',
    'Department_description' => 'Description',
    'Department_status' => 'Change Status',
    'Department_reason' => 'Status Reason',
    'Department_employees' => 'Number of employees',
    'Department_percentage' => 'Status Percentage',
    'Departments' => 'All Departments',
    'Performance' => 'Department Performance Range',
    'Edit' => 'Edit Department',
    'Delete' => 'Delete Confirmation',

    
   

    

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
