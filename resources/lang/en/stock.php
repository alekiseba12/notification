<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    //General

    'Stock_title' => 'Nyce Bar Stock Exchange',
    'Regions' => 'Counties Regions',
    'Service' => 'All Services',
    'Service_Slider' => 'Services Carousel',
    'Regions_Bars' => 'Bars Photos/Gallery',

    //Counties
    'View_counties' => 'View All Counties',
    'County_form' => 'County Form',
    'Counties' => 'Counties',
    'County' => 'Select County',

    //Regions
    'View_Regions' => 'View Regions',
    'Region_form' => 'Region Form',
    'Region' => 'Region Name',
    'Regions' => 'Regions',


    //Bars
    'View_Bars' => 'View Bars',
    'Bar'=>'Bar Name',
    'Bar_form'=>'Bar Form',
    'Bars' => 'Bars',


     
   //products
    'View_products' => 'View EABL Products',
    'Product_form' => 'Product Form',
    'Products' => 'EABL Products',
    'Eabl' => 'Select EABL Product',
    'Eabl_Stock' => 'Number of Stock',
    

    //General Trans
    'View_Bar_Products' => 'Bar EABL Products',
    'View_Region_Bars' => 'View Region Bars',
    'View_County_Regions' => 'View County Regions',


    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
