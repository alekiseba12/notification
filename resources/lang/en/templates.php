<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'Company_employees' => 'Employees',
    'Company_clients' => 'Clients',
    'Company_gadgets' => 'Gadgets',
    'Company_matatus' => 'Matatus',
    'Company_departments' => 'Departments',
    'Company_services' => 'Services',
    'Company_title' => 'Company Dashboard',
    'Company_gadgets' => 'Gadgets',
    'Matatus_Matatus' => 'Matatus',
    'Company_dashboard' => 'Dashboard',
    'Company_admin' => 'Admin',
    'Company_portfolio' => 'Company Profile',
    'Company_management' => 'Management Team',
    'Company_members' => 'Team Members',
    'Company_task' => 'Tasks',
    'Company_management' => 'Management Team',
    'Company_all_employees' => 'View All Employees',
    'Company_located_gadgets' => 'Company Wi-Fi Locations',
    'Company_all_gadgets' => 'View All  Wi-Fi Locations',
    'Company_all_services' => 'Company Services',
    'Add_Employee' => 'Add New Employee',
    'View_Employee' => 'View Employees',
    'Add_Client' => 'Add New Client',
    'View_Clients' => 'View Clients',
    'Employee_Attandance' => 'View Attandance',
    'Add_Department' => 'Add New Department',
    'View_Department' => 'View Departments',
    'Add_Gadget' => 'Add New Gadget',
    'View_Gadgets' => 'View Gadgets',
    'Add_Matatu' => 'Add New Matatu',
    'View_Matatu' => 'View Matatus',
    'Add_Service' => 'Add New Serivce',
    'View_Services' => 'View Services',
    'Company_played_advert' => 'Viewed Adverts',
    'Company_Adverts' => 'Adverts',
    'Add_Advert' => 'Add New Advert',
    'View_Advert' => 'View Adverts',
    'Company_Wifi' => 'Check Wifi',
    'Locate_Wifi' => 'Locate New Wifi',
    'View_Wifi' => 'View Wifi',
    'Add_Role' => 'Add New Role',
    'Roles' => 'Roles',
    'Company_Bars' => 'Bar Stock Exchange',
    'Company_main' => 'Company Management System',
    'Company_Clients' => 'Our Clients',
    'Company_all_clients'=>'All Clients',
    'Employees_info' => 'Employees Contacts',
    'Contacts' => 'View Contracts',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
