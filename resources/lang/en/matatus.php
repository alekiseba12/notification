<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'Matatu_title' => 'Matatus ',
    'Matatu_form' => 'Matatu Form',
    'Matatu_Name' => 'Name',
    'Matatu_No' => 'Number Plate',
    'Matatu_Routes' => 'Routes',
    'Matatu_Passangers'=> 'No Of Passangers',
    'Matatu_Edit'=> 'Edit Matatu',
    'Matatu_Delete'=> 'Delete Matatu',

];
