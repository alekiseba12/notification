<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    //Employee registration form
    'User_title' => 'Profile Info',
    'User_salary' => 'Salary Payment',
    'User_department' => 'Department',
    'User_date' => 'Select Date',
    'User_day' => 'Select Day',
    'User_time_in' => 'Time In',
    'User_time_out' => 'Time Out',
    'User_contracts' => 'Contracts',
    'User_Activation'=>'Activation Dashboard',
    'User_Activation_title'=>'Activation',
    'User_Page'=>' Page',
    'User_Alert'=>' Kindly wait as your profile is being activated by Admin',
  

    

    

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
