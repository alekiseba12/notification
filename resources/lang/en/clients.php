<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    //Employee registration form
    'Client_title' => 'Clients',
    'Client_form' => 'Client Form',
    'Client_phone' => 'Phone Number',
    'Client_email' => 'Email Address',
    'Client_percentage' => 'Activeness Percentage',
    'Client_address' => 'Postcode/AreaCode',
    'Client_location' => 'Location',
    'Client_company' => 'Company',
    'Client_logo' => 'Company Logo',
    'Client_service' => 'Service',
    'Client_status' => 'Change Status',
    'Active_Clients' => 'View Active Client',
    'Inactive_Clients' => 'View Inactive Client',
    'View_Client' => 'View Client Details',
    'Edit_Client' => 'Edit Client Details',
    'Delete_Client' => 'Delete Client ',

    

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
