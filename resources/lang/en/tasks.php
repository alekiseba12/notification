<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    //Adverts form
    'Task_title' => 'Tasks',
    'Task_form' => 'Tasks Form',
    'Task_name' => 'Task Name',
    'Task_start' => 'Start Date',
    'Task_end' => 'End Date',
    'Task_description' => 'Description',
    'Task_user' => 'User',
    'Task_status' => 'Status',
    'Edit' => 'Edit Task',
    'Delete' => 'Delete Task',
    'List' => 'All Tasks',
    'User' => 'Assigned task',
    'Response' => 'Task Feedback',
    'Status' => 'Change task status',
    'Feedback' => 'Provide feedback',
    'Reason' => 'Reason for uncomplete task',
    'Reason_feedback' => 'State reason',


    
   

    

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
