<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    //Employee registration form
    'Employee_title' => 'Employees',
    'Employee_form' => 'Employee Form',
    'Employee_team' => 'Company Management Team',
    'Employee_fullname' => 'Fullnames',
    'Employee_middlename' => 'Middlename',
    'Employee_lastname' => 'Lastname',
    'Employee_gender' => ' Provide Gender',
    'Employee_phone' => 'Phone Number',
    'Employee_email' => 'Company Email Address',
    'Employee_address' => 'Postcode/AreaCode',
    'Employee_location' => 'Location',
    'Employee_id' => 'National ID',
    'Employee_male' => 'Male',
    'Employee_type' => 'Employment Type',
    'Employee_salary' => 'Salary Scale',
    'Employee_pic' => 'Personal Picture',
    'Employee_role' => 'Employee Role',
    'Employee_department' => 'Employee Department',
    'Employee_user' => 'Registered User',
    'Employee_education' => 'Education Level',
    'Employee_skill' => 'Professional Skills',
    'Employee_description' => 'Insert your profile description here.',
    'Employee_profile' => 'Employee Profile Info',
    'Employee_reports' => 'Working Reports',
    'Employee_attandance' => 'Attendance Reports',
    'Employee_payment' => 'Payment Reports',
    'Employee_cases' => 'Disciplinary Cases',


    

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
